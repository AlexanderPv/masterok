import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

ThemeData masterOkTheme = ThemeData.light().copyWith(
    appBarTheme: const AppBarTheme(
      color: Color.fromRGBO(255, 204, 0, 1),
      titleTextStyle: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 1),
          fontSize: 20,
          fontWeight: FontWeight.w700,
          fontFamily: 'OpenSans-Bold',
          //height: 24,
          letterSpacing: -0.41),
      centerTitle: true,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(12),
        bottomRight: Radius.circular(12),
      )),
      systemOverlayStyle: SystemUiOverlayStyle(
         statusBarIconBrightness: Brightness.light,
       statusBarBrightness: Brightness.light,
          systemStatusBarContrastEnforced: false
       // statusBarColor: Color.fromRGBO(255, 204, 0, 1),
      ),
      surfaceTintColor: Colors.transparent,
      // ),
      //systemOverlayStyle: SystemUiOverlayStyle.light,
      iconTheme: IconThemeData(color: Colors.white),
    ),
    popupMenuTheme: PopupMenuThemeData(
      color: Colors.white,
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12), borderSide: BorderSide.none),
      textStyle: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w400,
          fontFamily: 'OpenSans',
          // height: 20,
          letterSpacing: -0.41,
          color: Colors.black),
    ),
    dividerTheme: const DividerThemeData(color: Color.fromRGBO(0, 0, 0, 0.3)),
    dialogTheme: DialogTheme(
    shape: OutlineInputBorder(borderRadius: BorderRadius.circular(12),
    borderSide: BorderSide.none)),
    searchBarTheme: const SearchBarThemeData(
        surfaceTintColor: null,
        shape: MaterialStatePropertyAll(RoundedRectangleBorder(
            side: BorderSide.none,
            borderRadius: BorderRadius.all(Radius.circular(12)))),
        backgroundColor: MaterialStatePropertyAll(
          Color.fromRGBO(0, 0, 0, 0.05),
        ),
      elevation: MaterialStatePropertyAll(0),
      //shadowColor: MaterialStatePropertyAll()
    ),
    textTheme: ThemeData.light().textTheme.copyWith(
          headlineMedium: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
              fontFamily: 'OpenSans-Bold',
              // height: 24,
              letterSpacing: -0.41,
              color: Colors.black),
          headlineSmall: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
              fontFamily: 'OpenSans',
              // height: 20,
              letterSpacing: -0.41,
              color: Colors.black),
          titleMedium: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontFamily: 'OpenSans',
              // height: 20,
              letterSpacing: -0.41,
              color: Colors.black),
          titleSmall: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w700,
              fontFamily: 'OpenSans',
              // height: 16,
              letterSpacing: -0.41,
              color: Colors.black),
          bodyMedium: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              fontFamily: 'OpenSans',
              // height: 16,
              letterSpacing: -0.41,
              color: Colors.black),
        ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: const MaterialStatePropertyAll(
          Color.fromRGBO(255, 204, 0, 1),
        ),
        // textStyle: const MaterialStatePropertyAll(TextStyle(
        //     fontSize: 16,
        //     fontWeight: FontWeight.w700,
        //     fontFamily: 'OpenSans',
        //     letterSpacing: -0.41,
        //     color: Colors.white)),
        shape: MaterialStatePropertyAll(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
      ),
    ));

extension CustomColor on ThemeData {
  Color get currentAppThemeColor => const Color.fromRGBO(255, 204, 0, 1);
}
