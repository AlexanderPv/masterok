import 'dart:io';
import 'package:flutter/material.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/data/db_queries.dart';
import 'package:master_ok_new/models/toolModel.dart';
import 'package:master_ok_new/models/manual.dart';
import 'package:master_ok_new/models/manual_tools.dart';
import 'package:master_ok_new/models/picture.dart';
import 'package:master_ok_new/models/manual_with_tools.dart';
import 'package:path_provider/path_provider.dart';
import 'package:master_ok_new/models/subject_model.dart';
import 'package:master_ok_new/models/article.dart';
import 'package:master_ok_new/models/article_subjects.dart';
import 'package:master_ok_new/models/article_with_subjectsAndPicture.dart';

class AppState {
  AppState._();

  static final AppState _instance = AppState._();

  static AppState get instance => _instance;

  late final DbQueries _dbQueries;
  bool created = false;
  late List<Project> projectList;
  List<Tool> toolsList = [];
  List<bool> chooseToolsList = [];
  List<Manual> manualList = [];
  List<ManualTools> currentManualToolsList = [];
  List<File> currentPictureList = [];
  List<Image> currentImageList = [];
  List<Picture> currentPictureModelsList = [];
  List<Tool> selectedTools = [];
  List<ManualWithTools> manualWithToolsList = [];
  List<File> fileFromImageList = [];

  ///СТАТЬИ

  List<bool> chooseSubjectList = [];
  List<Subject> subjectList = [];
  List<Article> articleList = [];
  List<ArticleSubjects> currentArticleSubjectList = [];
  List<ArticleWithSubjectsAndPicture> articleWithSubjectsList = [];
  List<Subject> selectedSubjects = [];

  initAppState() async {
    if (!created) {
      _dbQueries = await DbQueries.create();
      created = true;
    }
    await getProjects();
    await getManuals();
    await getArticles();
  }

  initManualInfo(int manualId) async {
    currentManualToolsList.clear();
    await getCurrentManualToolsList(manualId);
    await getCurrentPictures(manualId, 'manual');
    getChooseToolsList();
  }

  addNewProject(Project project) async {
    await _dbQueries.insertProject(project);
    await getProjects();
  }

  getProjects() async {
    projectList = await _dbQueries.getProjectList();
  }

  getCurrentManualToolsList(int manualId) async {
    currentManualToolsList = await _dbQueries.getManualToolsList(manualId);
  }

  getCurrentPictures(int sourceId, String source) async {
    //List<Picture> picList = await _dbQueries.getPictureList(manualId, source);
    //currentImageList = List.generate(
    //picList.length, (index) => File.fromRawPath(picList[index].picture));
    //picList.length, (index) => Image.memory(picList[index].picture));
    currentPictureModelsList =
        await _dbQueries.getPictureList(sourceId, source);
  }

  getManuals() async {
    manualList = await _dbQueries.getManualList();
  }

  updateProject(Project project) async {
    await _dbQueries.updateProject(project);
    await getProjects();
  }

  deleteProject(Project project) async {
    await _dbQueries.deleteProject(project.id!);
    await getProjects();
  }

  test() async {
    _dbQueries.deleteDataBase();
  }

  addNewTool(Tool tool) async {
    await _dbQueries.insertTool(tool);
    await getTools();
    chooseToolsList.add(false);
  }

  getTools() async {
    toolsList = await _dbQueries.getToolsList();
  }

  getChooseToolsList() {
    chooseToolsList = List.generate(toolsList.length, (index) => false);
  }

  deleteTool(Tool tool, int index) async {
    await _dbQueries.deleteTool(tool.id!);
    await getTools();
    chooseToolsList.removeAt(index);
  }

  initToolsList() async {
    await getTools();
    getChooseToolsList();
  }

  initManualsList() async {
    await initToolsList();
    await getManuals();
    await getManualWithToolsList();
    clearSelectedTools();
  }

  setChooseToolsListItem(int index, bool choose) {
    chooseToolsList[index] = choose;
  }

  addNewManual(Manual manual) async {
    int manualId = await _dbQueries.insertManual(manual);
    for (int i = 0; i < chooseToolsList.length; i++) {
      if (chooseToolsList[i]) {
        await _dbQueries.insertManualTools(ManualTools(
            manualId: manualId,
            toolId: toolsList[i].id!,
            toolTitle: toolsList[i].title));
      }
    }
    for (int j = 0; j < currentPictureList.length; j++) {
      await _dbQueries.insertPicture(Picture(
          sourceId: manualId,
          picture: await currentPictureList[j].readAsBytes(),
          source: 'manual'));
    }
    chooseToolsList.clear();
    currentPictureList.clear();
    //currentManualToolsList.clear();
  }

  addPicture(List<File> pictures) {
    currentPictureList = List.from(pictures);
  }

  getSelectedTools() {
    clearSelectedTools();
    for (int i = 0; i < chooseToolsList.length; i++) {
      if (chooseToolsList[i]) {
        selectedTools.add(toolsList[i]);
      }
    }
  }

  clearSelectedTools() {
    selectedTools.clear();
  }

  deleteSelectedTool(int selectedToolId) {
    selectedTools.removeAt(selectedToolId);
  }

  clearChooseToolsList() {
    getChooseToolsList();
  }

  Future<void> getManualWithToolsList() async {
    manualWithToolsList.clear();
    for (var e in manualList) {
      manualWithToolsList.add(ManualWithTools(
          id: e.id,
          title: e.title,
          description: e.description,
          complexity: e.complexity,
          videoLinks: e.videoLinks,
          mainText: e.mainText,
          manualTools: await _dbQueries.getManualToolsList(e.id!)));
    }
  }

  Future<void> deleteManual(Manual manual) async {
    await _dbQueries.deleteManual(manual.id!);
    await getManuals();
    await getManualWithToolsList();
  }

  Future<void> initEditManual() async {
    fileFromImageList.clear();

    for (int i = 0; i < currentPictureModelsList.length; i++) {
      final tempDir = await getTemporaryDirectory();
      final tempPath = tempDir.path;
      File file = File('$tempPath/image$i.png');
      await file.writeAsBytes(currentPictureModelsList[i].picture);
      fileFromImageList.add(file);
    }
  }

  Future<void> updateManual(Manual manual) async {
    await _dbQueries.updateManual(manual);
    for (int i = 0; i < chooseToolsList.length; i++) {
      if (chooseToolsList[i]) {
        await _dbQueries.insertManualTools(ManualTools(
            manualId: manual.id!,
            toolId: toolsList[i].id!,
            toolTitle: toolsList[i].title));
      }
    }
    for (int j = 0; j < currentPictureList.length; j++) {
      await _dbQueries.insertPicture(Picture(
          sourceId: manual.id!,
          picture: await currentPictureList[j].readAsBytes(),
          source: 'manual'));
    }
    chooseToolsList.clear();
    currentPictureList.clear();
  }

  ///СТАТЬИ

  initSubjectList() async {
    await getSubjects();
    getChooseSubjectList();
  }

  getSubjects() async {
    subjectList = await _dbQueries.getSubjectList();
  }

  getChooseSubjectList() {
    chooseSubjectList = List.generate(subjectList.length, (index) => false);
  }

  deleteSubject(Subject subject, int index) async {
    await _dbQueries.deleteSubject(subject.id!);
    await getSubjects();
    chooseSubjectList.removeAt(index);
  }

  setChooseSubjectListItem(int index, bool choose) {
    chooseSubjectList[index] = choose;
  }

  addNewSubject(Subject subject) async {
    await _dbQueries.insertSubject(subject);
    await getSubjects();
    chooseSubjectList.add(false);
  }

  addNewArticle(Article article) async {
    int articleId = await _dbQueries.insertArticle(article);
    for (int i = 0; i < chooseSubjectList.length; i++) {
      if (chooseSubjectList[i]) {
        await _dbQueries.insertArticleSubjects(ArticleSubjects(
            articleId: articleId,
            subjectId: subjectList[i].id!,
            subjectTitle: subjectList[i].title));
      }
    }
    for (int j = 0; j < currentPictureList.length; j++) {
      await _dbQueries.insertPicture(Picture(
          sourceId: articleId,
          picture: await currentPictureList[j].readAsBytes(),
          source: 'article'));
    }
    chooseToolsList.clear();
    currentPictureList.clear();
    //currentManualToolsList.clear();
  }

  getArticles() async {
    articleList = await _dbQueries.getArticleList();
    if (articleList.isNotEmpty) {
      await getCurrentPictures(articleList[0].id!, 'article');
      await getCurrentArticleSubjectList(articleList[0].id!);
    }
  }

  initArticleInfo(int articleId) async {
    currentArticleSubjectList.clear();
    await getCurrentArticleSubjectList(articleId);
    await getCurrentPictures(articleId, 'article');
    getChooseSubjectList();
  }

  getCurrentArticleSubjectList(int articleId) async {
    currentArticleSubjectList =
        await _dbQueries.getArticleSubjectList(articleId);
  }

  initArticleList() async {
    await initSubjectList();
    await getArticles();
    await getArticleWithSubjectsAndPictureList();
    clearSelectedSubjects();
  }

  Future<void> getArticleWithSubjectsAndPictureList() async {
    articleWithSubjectsList.clear();
    for (var e in articleList) {
      await getCurrentPictures(e.id!, 'article');
      articleWithSubjectsList.add(ArticleWithSubjectsAndPicture(
          id: e.id,
          title: e.title,
          description: e.description,
          videoLinks: e.videoLinks,
          mainText: e.mainText,
          articleSubjects: await _dbQueries.getArticleSubjectList(e.id!),
          picture: currentPictureModelsList.isNotEmpty
              ? currentPictureModelsList[0]
              : null));
    }
  }

  clearSelectedSubjects() {
    selectedSubjects.clear();
  }

  clearChooseSubjectList() {
    getChooseSubjectList();
  }

  getSelectedSubjects() {
    clearSelectedSubjects();
    for (int i = 0; i < chooseSubjectList.length; i++) {
      if (chooseSubjectList[i]) {
        selectedSubjects.add(subjectList[i]);
      }
    }
  }

  deleteSelectedSubject(int selectedSubjectId) {
    selectedSubjects.removeAt(selectedSubjectId);
  }

  Future<void> deleteArticle(Article article) async {
    await _dbQueries.deleteArticle(article.id!);
    await getArticles();
    await getArticleWithSubjectsAndPictureList();
  }

  Future<void> initEditArticle() async {
    fileFromImageList.clear();

    for (int i = 0; i < currentPictureModelsList.length; i++) {
      final tempDir = await getTemporaryDirectory();
      final tempPath = tempDir.path;
      File file = File('$tempPath/image$i.png');
      await file.writeAsBytes(currentPictureModelsList[i].picture);
      fileFromImageList.add(file);
    }
  }

  Future<void> updateArticle(Article article) async {
    await _dbQueries.updateArticle(article);
    for (int i = 0; i < chooseSubjectList.length; i++) {
      if (chooseSubjectList[i]) {
        await _dbQueries.insertArticleSubjects(ArticleSubjects(
            articleId: article.id!,
            subjectId: subjectList[i].id!,
            subjectTitle: subjectList[i].title));
      }
    }
    for (int j = 0; j < currentPictureList.length; j++) {
      await _dbQueries.insertPicture(Picture(
          sourceId: article.id!,
          picture: await currentPictureList[j].readAsBytes(),
          source: 'article'));
    }
    chooseSubjectList.clear();
    currentPictureList.clear();
  }

  }

