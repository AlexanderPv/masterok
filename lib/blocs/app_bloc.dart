import 'dart:async';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:sqflite/sql.dart';

class AppBloc {
   final AppState _currentState = AppState.instance;

  final _stateController = StreamController<AppState>();
  final _actionsController = StreamController<ActionEvent>();

  Stream<AppState> get state => _stateController.stream;
  Sink<ActionEvent> get action => _actionsController.sink;

  AppBloc() {
    _actionsController.stream.listen(handleAction);
  }

  void dispose(){
    _stateController.close();
    _actionsController.close();
  }

  Future<void> handleAction(ActionEvent action) async {
    if(action is InitApp){
      await _currentState.initAppState();
      _stateController.add(_currentState);
    }
    else if(action is AddNewProject) {
      await _currentState.addNewProject(action.project);
      //_stateController.add(_currentState);
    }
    else if(action is UpdateProject) {
      await _currentState.updateProject(action.project);
      //_stateController.add(_currentState);
    }
    else if(action is DeleteProject){
      await _currentState.deleteProject(action.project);
    }

    /// technical test
    else if(action is Test){
      await _currentState.test();
      _stateController.add(_currentState);
    }

    else if(action is AddNewTool) {
      await _currentState.addNewTool(action.tool);
      _stateController.add(_currentState);
    }

    else if(action is GetTools) {
      await _currentState.getTools();
      _stateController.add(_currentState);
    }

    else if(action is InitToolsList) {
      await _currentState.initToolsList();
      _stateController.add(_currentState);
    }

    else if(action is DeleteTool){
      await _currentState.deleteTool(action.tool, action.index);
      _stateController.add(_currentState);
    }

    else if(action is SetChooseToolsListItem){
      _currentState.setChooseToolsListItem(action.index, action.choose);
      _stateController.add(_currentState);
    }

    else if(action is AddNewManual){
      await _currentState.addNewManual(action.manual);
    }

    else if(action is InitManualInfo){
      await _currentState.initManualInfo(action.manualID);
    }

    else if(action is GetStreamForManualInfo){
      _stateController.add(_currentState);
    }

    else if(action is AddPicture) {
      _currentState.addPicture(action.pictures);
      //_stateController.add(_currentState);
    }

    else if(action is InitManualsList){
      await _currentState.initManualsList();
      _stateController.add(_currentState);
    }

    else if(action is GetSelectedTools){
      _currentState.getSelectedTools();
      _stateController.add(_currentState);
    }

    else if(action is ClearSelectedTools){
      _currentState.clearSelectedTools();
      _stateController.add(_currentState);
    }
    else if(action is DeleteSelectedTool){
      _currentState.deleteSelectedTool(action.selectedToolId);
      _stateController.add(_currentState);
    }
    else if(action is ClearChooseToolsList){
      _currentState.clearChooseToolsList();
      _stateController.add(_currentState);
    }
    else if(action is GetManualWithToolsList){
      await _currentState.getManualWithToolsList();
      _stateController.add(_currentState);
    }

    else if(action is DeleteManual){
      _currentState.deleteManual(action.manual);
      _stateController.add(_currentState);
    }

    else if(action is InitEditManual){
      await _currentState.initEditManual();
      _stateController.add(_currentState);
    }

    else if(action is UpdateManual){
      await _currentState.updateManual(action.manual);
      //_stateController.add(_currentState);
    }

    ///СТАТЬИ

    else if(action is InitSubjectList) {
      await _currentState.initSubjectList();
      _stateController.add(_currentState);
    }

    else if(action is DeleteSubject){
      await _currentState.deleteSubject(action.subject, action.index);
      _stateController.add(_currentState);
    }

    else if(action is SetChooseSubjectListItem){
      _currentState.setChooseSubjectListItem(action.index, action.choose);
      _stateController.add(_currentState);
    }

    else if(action is AddNewSubject) {
      await _currentState.addNewSubject(action.subject);
      _stateController.add(_currentState);
    }

    else if(action is AddNewArticle){
      await _currentState.addNewArticle(action.article);
    }

    else if(action is InitArticleInfo){
      await _currentState.initArticleInfo(action.articleId);
    }

    else if(action is InitArticleList){
      await _currentState.initArticleList();
      _stateController.add(_currentState);
    }

    else if(action is ClearSelectedSubjects){
      _currentState.clearSelectedSubjects();
      _stateController.add(_currentState);
    }

    else if(action is ClearChooseSubjectList){
      _currentState.clearChooseSubjectList();
      _stateController.add(_currentState);
    }

    else if(action is GetSelectedSubjects){
      _currentState.getSelectedSubjects();
      _stateController.add(_currentState);
    }

    else if(action is DeleteSelectedSubject){
      _currentState.deleteSelectedSubject(action.selectedSubjectId);
      _stateController.add(_currentState);
    }

    else if(action is GetStreamForArticleInfo){
      _stateController.add(_currentState);
    }

    else if(action is DeleteArticle){
      _currentState.deleteArticle(action.article);
      _stateController.add(_currentState);
    }

    else if(action is InitEditArticle){
      await _currentState.initEditArticle();
      _stateController.add(_currentState);
    }

    else if(action is UpdateArticle){
      await _currentState.updateArticle(action.article);
    }
  }
}
