import 'package:flutter/material.dart';
import 'package:master_ok_new/models/news_model.dart';

class NewsText extends StatelessWidget {
  const NewsText({super.key,});

  @override
  Widget build(BuildContext context) {
    final News news =
    ModalRoute.of(context)!.settings.arguments as News;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Новость'),
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    clipBehavior: Clip.hardEdge,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12),
                          topRight: Radius.circular(12)),
                    ),
                    width: double.infinity,
                    height: 150,
                    child: Image.asset(
                      news.picture,
                      fit: BoxFit.fitWidth,
                    )),
                Text(
                  news.title,
                  style: Theme.of(context)
                      .textTheme
                      .headlineSmall,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Text(news.text,style: Theme.of(context).textTheme.titleMedium),
                ),
              ],
            ),
      ),
    ));
  }
}
