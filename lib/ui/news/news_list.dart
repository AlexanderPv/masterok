import 'package:flutter/material.dart';
import 'package:master_ok_new/data/get_json.dart';
import 'package:master_ok_new/ui/news/news_box.dart';

class NewsList extends StatelessWidget {
  const NewsList({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Новости'),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: getNewsFromJson("assets/json/news1.json"),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              return const Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.waiting:
              return const Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.none:
              return const Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.done:
              return Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(
                    children: snapshot.data != null
                        ? snapshot.data!
                        .map((e) => NewsBox(news: e))
                        .toList()
                        : []),
              );
          }
        },
      )
    );
  }
}
