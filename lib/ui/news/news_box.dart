import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:master_ok_new/models/news_model.dart';

class NewsBox extends StatelessWidget {
  const NewsBox({super.key, required this.news});

  final News news;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed('/newsText', arguments: news);
      },
      child: Container(
        margin: const EdgeInsets.only(top: 12),
        height: 142,
        width: double.infinity,
        padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: const [
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.25),
                    offset: Offset(10, 10),
                    blurRadius: 35,
                    spreadRadius: -15,
                    blurStyle: BlurStyle.normal)
              ]),
        child: Column(
          children: [
            Container(
                clipBehavior: Clip.antiAlias,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(12),
                      topRight: Radius.circular(12)),
                ),
                height: 66,
                width: double.infinity,
                child: Image.asset(
                  news.picture,
                  fit: BoxFit.fitWidth,
                )),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                news.text,
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ),
    );
  }
}
