import 'package:flutter/material.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:master_ok_new/ui/settings/rating_dialog.dart';

class Settings extends StatelessWidget {
  const Settings({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Настройки'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                GestureDetector(
                  onTap: () async {
                    final url = Uri.parse(
                      'https://www.flutter.com/privacy-policy/',
                    );
                    if (await canLaunchUrl(url)) {
                    launchUrl(url);
                    }
                  },
                  child: Container(
                    height: 60,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.25),
                              offset: Offset(10, 10),
                              blurRadius: 35,
                              spreadRadius: -15,
                              blurStyle: BlurStyle.normal)
                        ]),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Политика конфиденциальности',
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              size: 20,
                              color: Theme.of(context).currentAppThemeColor,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    final url = Uri.parse(
                      'https://docs.flutter.dev/tos',
                    );
                    if (await canLaunchUrl(url)) {
                    launchUrl(url);
                    }
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 12),
                    height: 60,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.25),
                              offset: Offset(10, 10),
                              blurRadius: 35,
                              spreadRadius: -15,
                              blurStyle: BlurStyle.normal)
                        ]),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Пользовательское соглашение',
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              size: 20,
                              color: Theme.of(context).currentAppThemeColor,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                ),
                  onPressed: () {
                    showDialog(
                      context: context,
                      barrierDismissible: true,
                      builder: (context) => ratingDialog,
                    );
                  },
                  child: Text(
                    'Оценить приложение',
                    style: Theme.of(context)
                        .textTheme
                        .headlineSmall
                        ?.copyWith(color: Colors.white),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
