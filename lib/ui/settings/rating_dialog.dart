import 'package:rating_dialog/rating_dialog.dart';
import 'package:flutter/material.dart';

final ratingDialog = RatingDialog(
  initialRating: 1.0,
  title: const Text(
    'Оцените приложение',
    textAlign: TextAlign.center,
    style: TextStyle(
      fontSize: 25,
      fontWeight: FontWeight.bold,
    ),
  ),
  message: const Text(
    'Нажмите на звездочку, чтобы выставить свой рейтинг.',
    textAlign: TextAlign.center,
    style: TextStyle(fontSize: 15),
  ),
  // image:
  submitButtonText: 'Подтвердить',
  commentHint: 'Оставьте комментарий',
  onCancelled: () => {
    /// some logic
  },
  onSubmitted: (response) {
    /// some logic
  },
);
