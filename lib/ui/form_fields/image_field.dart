import 'dart:io';
import 'package:flutter/material.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:image_picker/image_picker.dart';

class ImageField extends StatefulWidget {
  const ImageField({super.key, required this.images});

  final List <File> images;

  @override
  State<ImageField> createState() => _ImageFieldState();
}

class _ImageFieldState extends State<ImageField> {
  late final AppBloc bloc;
  late final ImagePicker imagePicker;

  @override
  void initState() {
    bloc = AppBloc();
    imagePicker = ImagePicker();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppState>(
        stream: bloc.state,
        builder: (context, snapshot) {
          return Padding(
            padding: const EdgeInsets.only(top: 12),
            child: Column(children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Фотографии',
                    style: Theme.of(context).textTheme.bodyMedium,
                  )),
              Row(
                children: [
                  GestureDetector(
                    onTap: () async {
                      if (widget.images.length < 4) {
                        final XFile? image = await imagePicker.pickImage(
                            source: ImageSource.gallery, imageQuality: 50);
                        if (image != null) {
                          setState(() {
                            widget.images.add(File(image.path));
                          });
                        }
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(0, 0, 0, 0.05),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      height: 60,
                      width: 60,
                      child: const Icon(
                        Icons.add,
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        size: 24,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Text(
                      'Добавить фотографии',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Row(
                  children: List.generate(
                      widget.images.length,
                      (index) => Padding(
                            padding: const EdgeInsets.only(right: 12),
                            child: Stack(
                              clipBehavior: Clip.none,
                                children: [
                              Container(
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                  color: const Color.fromRGBO(0, 0, 0, 0.05),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                height: 60,
                                width: 60,
                                child:
                                Image.file(widget.images[index],
                                  width: 60,
                                  height: 60,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Positioned(
                                bottom: -6,
                                right: -6,
                                child: GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      widget.images.removeAt(index);
                                    });
                                  },
                                  child: const CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 12,
                                    child: Icon(
                                      Icons.cancel,
                                      size: 24,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              )
                            ]),
                          )
                  ),
                ),
              )
            ]),
          );
        });
  }
}
