import 'package:flutter/material.dart';
import 'package:master_ok_new/data/utils.dart';

class ComplexPropertyField extends StatefulWidget {
  const ComplexPropertyField({super.key, required this.fieldName, required this.fieldController});

  final String fieldName;
  final TextEditingController fieldController;

  @override
  State<ComplexPropertyField> createState() => _ComplexPropertyFieldState();
}

class _ComplexPropertyFieldState extends State<ComplexPropertyField> {

  bool initialBuilt = false;

  final Icon i1 =
  const Icon(Icons.circle, size: 18, color: Color.fromRGBO(255, 204, 0, 1),key: Key('i1'),);
  final Icon i2 =
  const Icon(Icons.circle, size: 18, color: Color.fromRGBO(0, 0, 0, 0.3),key: Key('i2'));

  final dotList = List<Icon>.filled(
    5,
    const Icon(
      Icons.circle,
      size: 18,
      color: Color.fromRGBO(0, 0, 0, 0.3),
    ),
  );

  void updateComplexity(int index){
    for (int i = 0; i < dotList.length; i++) {
      if (i <= index) {
        dotList[i] = i1;
      } else {
        dotList[i] = i2;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if(!initialBuilt){
      if(widget.fieldController.text!=''){
        setState(() {
          updateComplexity(int.parse(widget.fieldController.text));
        });
      }
      initialBuilt = true;
    }
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: Column(
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                widget.fieldName,
                style: Theme.of(context).textTheme.bodyMedium,
              )),
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Row(
              children: List.generate(
                  5,
                      (index) => Padding(
                      padding: const EdgeInsets.only(right: 28),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            updateComplexity(index);
                          });
                          widget.fieldController.text = index.toString();
                        },
                        child: dotList[index],
                      ))),
            ),
          )
        ],
      ),
    );
  }
}
