import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:master_ok_new/data/utils.dart';

class MainTextFieldForEdit extends StatelessWidget {
  const MainTextFieldForEdit(
      {super.key,
      required this.fieldName,
      required this.fieldController,
      // required this.fieldKey,
      required this.hintText,
      required this.validate});

  final String fieldName;
  final TextEditingController fieldController;

  // final GlobalKey<FormFieldState> fieldKey;
  final String hintText;
  final bool validate;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: Column(
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                fieldName,
                style: Theme.of(context).textTheme.bodyMedium,
              )),
          TextFormField(
            style: Theme.of(context).textTheme.titleMedium,
            textInputAction: TextInputAction.newline,
            // key: fieldKey,
            controller: fieldController,
            validator: validate ? Validating.simpleValidate : null,
            maxLength: 3000,
            minLines: 8,
            maxLines: 1000,
            decoration: InputDecoration(
                hintText: hintText,
                hintStyle: Theme.of(context)
                    .textTheme
                    .titleMedium
                    ?.copyWith(color: const Color.fromRGBO(0, 0, 0, 0.6)),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide:
                      BorderSide(color: Theme.of(context).currentAppThemeColor),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide:
                  BorderSide(color: Theme.of(context).currentAppThemeColor),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide:
                      BorderSide(color: Theme.of(context).currentAppThemeColor),
                ),
                contentPadding: const EdgeInsets.all(12)),
          ),
        ],
      ),
    );
  }
}
