import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Timer(
      const Duration(seconds: 3),
          () => Navigator.of(context).pushReplacementNamed('/intro1'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              height: 48,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 110),
              child: Image.asset(
                'assets/images/splash.png',
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fitHeight,
              ),
            ),
            Text(
              'Приложение для ремонтных \nработ',
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium
                  ?.copyWith(color: Colors.black),
              textAlign: TextAlign.center,
            ),

            // ElevatedButton(onPressed: () {
            //   Navigator.of(context).pushNamed('/main');
            // },
            //     child: Text('Переход')),
            // ElevatedButton(
            //     onPressed: (){
            //       Navigator.of(context).pushNamed('/technical');
            //     },
            //     child: Text('тех страница')),
          ],
        ),
      ),
    );
  }
}
