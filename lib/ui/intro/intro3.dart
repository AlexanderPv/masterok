import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Intro3 extends StatelessWidget {
  const Intro3({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Image.asset('assets/images/intro3.png',
          height: MediaQuery.of(context).size.height,
          fit: BoxFit.fitHeight,
          color: Colors.black.withOpacity(1.0),
          colorBlendMode: BlendMode.softLight,),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Colors.transparent,
                    Colors.black],
              begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              )
            ),
          ),
          Positioned(
            bottom: 32,
            child: Column(
              children: [
                Text(
                  'Пишите полезные  статьи',
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium
                      ?.copyWith(color: Colors.white),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Image.asset('assets/images/IntroDots3.png'),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width-40,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/main');
                        },
                        child: Text(
                          'Продолжить',
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall
                              ?.copyWith(color: Colors.white),
                        )),
                  ),
                )

              ],
            ),
          ),
    ]
      ),
    );

  }
}
