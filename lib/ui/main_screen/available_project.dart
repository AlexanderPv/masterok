import 'package:flutter/material.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';

class AvailableProject extends StatelessWidget {
  const AvailableProject(
      {super.key, required this.sizeLength, required this.project});

  final double sizeLength;
  final Project project;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed('/aboutProject', arguments: project);
      },
      child: Container(
        margin: const EdgeInsets.only(left: 12),
        width: sizeLength,
        height: sizeLength,
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: const [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.25),
                  offset: Offset(10, 10),
                  blurRadius: 35,
                  spreadRadius: -15,
                  blurStyle: BlurStyle.normal)
            ]),
        child: Text(
          project.title,
          textAlign: TextAlign.start,
          style: Theme.of(context).textTheme.headlineSmall,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
