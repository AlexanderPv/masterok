import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/models/article.dart';
import 'package:master_ok_new/models/picture.dart';
import 'package:master_ok_new/models/article_subjects.dart';
import 'package:master_ok_new/themes/app_theme.dart';

class AvailableArticle extends StatefulWidget {
  const AvailableArticle(
      {super.key, required this.article, this.picture, this.subjectList});

  final Article article;
  final Picture? picture;
  final List<ArticleSubjects>? subjectList;

  @override
  State<AvailableArticle> createState() => _AvailableArticleState();
}

class _AvailableArticleState extends State<AvailableArticle> {
  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppState>(
        stream: bloc.state,
        builder: (context, snapshot) {
          return GestureDetector(
            onTap: () {
              bloc.action.add(InitArticleInfo(widget.article.id!));
              Navigator.of(context)
                  .pushNamed('/aboutArticle', arguments: widget.article);
            },
            child: Container(
              margin: const EdgeInsets.only(top: 12),
              height: 150,
              width: double.infinity,
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: const [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        offset: Offset(10, 10),
                        blurRadius: 35,
                        spreadRadius: -15,
                        blurStyle: BlurStyle.normal)
                  ]),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 12),
                    child: Container(
                      clipBehavior: Clip.antiAlias,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              bottomLeft: Radius.circular((12)))),
                      child: widget.picture != null
                          ? Image.memory(
                              widget.picture!.picture,
                              width: 80,
                              height: 126,
                              fit: BoxFit.fitHeight,
                            )
                          : const SizedBox.shrink(),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.article.title,
                          textAlign: TextAlign.start,
                          style: Theme.of(context).textTheme.headlineSmall,
                          overflow: TextOverflow.ellipsis,
                        ),

                        /// СПИСОК ТЕМ

                        widget.subjectList != null
                            ? Align(
                          alignment: Alignment.centerRight,
                              child: Wrap(
                                  spacing: 12,
                                  runSpacing: 12,
                                  children: List.generate(
                                      widget.subjectList!.length,
                                      (index) => Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                                border: Border.all(
                                                    color: Theme.of(context)
                                                        .currentAppThemeColor)),
                                            height: 22,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 12, right: 12),
                                              child: Text(
                                                widget.subjectList![index]
                                                    .subjectTitle,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleSmall
                                                    ?.copyWith(
                                                      color: Theme.of(context)
                                                          .currentAppThemeColor,
                                                    ),
                                              ),
                                            ),
                                          )),
                                ),
                            )
                            : const SizedBox.shrink(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
