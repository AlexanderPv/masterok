import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:master_ok_new/themes/app_theme.dart';

class AddProjectBrick extends StatelessWidget {
  const AddProjectBrick(
      {super.key, required this.onTap, required this.sideLength});

  final Function() onTap;
  final double sideLength;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: sideLength,
        height: sideLength,
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Theme.of(context).currentAppThemeColor,
            borderRadius: BorderRadius.circular(12)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              textAlign: TextAlign.start,
              'Добавить проект',
              style: Theme.of(context)
                  .textTheme
                  .headlineSmall
                  ?.copyWith(color: Colors.white),
            ),
            Align(
                alignment: Alignment.centerRight,
                child: SvgPicture.asset('assets/icons/LargePlus.svg'))
          ],
        ),
      ),
    );
  }
}
