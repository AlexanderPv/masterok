import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/ui/main_screen/available_project.dart';
import 'package:master_ok_new/ui/main_screen/add_project_brick.dart';
import 'package:master_ok_new/ui/main_screen/available_manual.dart';
import 'package:master_ok_new/ui/main_screen/available_article.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final double outerPadding = 20;
  final double betweenPadding = 12;
  late double shortSize13;

  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
    bloc.action.add(InitApp());
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    shortSize13 = (MediaQuery.of(context).size.width -
            outerPadding * 2 -
            betweenPadding * 2) /
        3;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Главный экран'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/newsList');
            },
            icon: SvgPicture.asset('assets/icons/bell.fill.svg'),
            alignment: Alignment.centerRight,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/settings');
              },
              icon: SvgPicture.asset('assets/icons/gearshape.fill.svg'),
              alignment: Alignment.centerLeft,
            ),
          ),
        ],
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Проекты',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushNamed('/projectList');
                          },
                          child: Row(
                            children: [
                              Text(
                                'еще',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall
                                    ?.copyWith(
                                        color: Theme.of(context)
                                            .currentAppThemeColor),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Theme.of(context).currentAppThemeColor,
                                  size: 16,
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),

                    /// ПРОЕКТЫ

                    Padding(
                      padding: const EdgeInsets.only(top: 22),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          /// ДОБАВИТЬ ПРОЕКТ
                          AddProjectBrick(
                              onTap: () {
                                Navigator.of(context)
                                    .pushNamed('/addProject', arguments: bloc);
                              },
                              sideLength: shortSize13),

                          /// СПИСОК ПРОЕКТОВ

                          ...List.generate(2, (index) {
                            if (snapshot.data!.projectList.length > index) {
                              return AvailableProject(
                                sizeLength: shortSize13,
                                project: snapshot.data!.projectList[index],
                              );
                            } else {
                              return const SizedBox.shrink();
                            }
                          }),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(top: 22),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Инструкции',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, '/manualsList');
                            },
                            child: Row(
                              children: [
                                Text(
                                  'еще',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall
                                      ?.copyWith(
                                          color: Theme.of(context)
                                              .currentAppThemeColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color:
                                        Theme.of(context).currentAppThemeColor,
                                    size: 16,
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),

                    const SizedBox(
                      height: 9,
                    ),

                    ///СПИСОК ИНСТРУКЦИЙ
                    ...List.generate(2, (index) {
                      if (snapshot.data!.manualList.length > index) {
                        return AvailableManual(
                          manual: snapshot.data!.manualList[index],
                        );
                      } else {
                        return const SizedBox.shrink();
                      }
                    }),

                    ///СТАТЬИ

                    Padding(
                      padding: const EdgeInsets.only(top: 22),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Статьи',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, '/articleList');
                            },
                            child: Row(
                              children: [
                                Text(
                                  'еще',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall
                                      ?.copyWith(
                                          color: Theme.of(context)
                                              .currentAppThemeColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color:
                                        Theme.of(context).currentAppThemeColor,
                                    size: 16,
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 9,
                    ),

                    ///СПИСОК СТАТЕЙ
                    snapshot.data!.articleList.isNotEmpty
                        ? AvailableArticle(
                            article: snapshot.data!.articleList[0],
                            picture: snapshot.data!.currentPictureModelsList.isNotEmpty
                                ? snapshot.data!.currentPictureModelsList[0]
                                : null,
                            subjectList: snapshot.data!.currentArticleSubjectList.isNotEmpty
                                ? snapshot.data!.currentArticleSubjectList
                                : null,
                          )
                        : const SizedBox.shrink(),
                  ],
                ),
              );
            }
          }),
    );
  }
}
