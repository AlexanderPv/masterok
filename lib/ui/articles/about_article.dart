import 'package:flutter/material.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/ui/manuals/image_popup.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:master_ok_new/models/article.dart';
import 'package:master_ok_new/ui/articles/article_popup_menu.dart';

class AboutArticle extends StatefulWidget {
  const AboutArticle({super.key});

  @override
  State<AboutArticle> createState() => _AboutArticleState();
}

class _AboutArticleState extends State<AboutArticle> {
  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
    bloc.action.add(GetStreamForArticleInfo());
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Article article = ModalRoute.of(context)!.settings.arguments as Article;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Статья'),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 12),
            child: ArticlePopUpMenu(
              width: MediaQuery.of(context).size.width,
              article: article,

            ),
          ),
        ],
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'О статье',
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Название',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          article.title,
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Text(
                          'Короткое описание',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          article.description ?? '',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Text(
                          'Тематика статьи',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(top: 4),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: List.generate(
                                  snapshot.data!.currentArticleSubjectList.length,
                                  (index) =>
                                      // Text(index.toString())
                                      Text(
                                          '  ${index + 1}. ${snapshot.data!.currentArticleSubjectList[index].subjectTitle}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleMedium)))),

                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Text(
                          'Статья',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Фотографии',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Row(
                          children: List.generate(
                              snapshot.data!.currentPictureModelsList.length,
                              (index) => Padding(
                                    padding: const EdgeInsets.only(right: 12),
                                    child: GestureDetector(
                                      onTap: () {
                                        showModalBottomSheet(
                                            clipBehavior: Clip.antiAlias,
                                            context: context,
                                            isScrollControlled: true,
                                            isDismissible: false,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(12)),
                                            builder: (BuildContext context) {
                                              return DraggableScrollableSheet(
                                                initialChildSize: 0.85,
                                                expand: false,
                                                builder: (BuildContext context,
                                                    ScrollController
                                                        scrollController) {
                                                  return ImagePopup(
                                                    pictureList: snapshot.data!
                                                        .currentPictureModelsList,
                                                    currentIndex: index,
                                                  );
                                                },
                                              );
                                            });
                                      },
                                      child: Container(
                                        clipBehavior: Clip.antiAlias,
                                        decoration: BoxDecoration(
                                          color:
                                              const Color.fromRGBO(0, 0, 0, 0.05),
                                          borderRadius: BorderRadius.circular(12),
                                        ),
                                        height: 60,
                                        width: 60,
                                        child: Image.memory(
                                          snapshot
                                              .data!
                                              .currentPictureModelsList[index]
                                              .picture,
                                          width: 60,
                                          height: 60,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Ссылка на видеоматериалы',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: GestureDetector(
                          onTap: () async {
                            if (article.videoLinks != null) {
                              final url = Uri.parse(
                                article.videoLinks!,
                              );
                              if (await canLaunchUrl(url)) {
                                launchUrl(url);
                              }
                            }
                          },
                          child: Text(
                            article.videoLinks ?? '',
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    color:
                                        Theme.of(context).currentAppThemeColor),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Текст статьи',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          article.mainText ?? '',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          }),
    );
  }
}
