import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:master_ok_new/models/article_with_subjectsAndPicture.dart';
import 'package:master_ok_new/ui/articles/available_article_for_list.dart';
import 'package:master_ok_new/ui/articles/subjects_popup.dart';

class ArticleList extends StatefulWidget {
  const ArticleList({super.key});

  @override
  State<ArticleList> createState() => _ArticleListState();
}

class _ArticleListState extends State<ArticleList> {
  late final AppBloc bloc;
  String searchString = '';

  int selectedComplexity = 0;

  bool getSubjectsMatch(ArticleWithSubjectsAndPicture articleWithSubjects,
      AsyncSnapshot<AppState> snapshot) {
    bool match = true;
    if (snapshot.data!.selectedSubjects.isEmpty) {
      return true;
    } else if (articleWithSubjects.articleSubjects!.isEmpty) {
      return false;
    } else {
      for (var e in snapshot.data!.selectedSubjects) {
        bool x = false;
        for (var o in articleWithSubjects.articleSubjects!) {
          if (e.id == o.subjectId) {
            x = x || true;
          }
        }
        match = match && x;
      }
    }
    return match;
  }

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
    bloc.action.add(InitArticleList());
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Статьи'),
        centerTitle: true,
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                              ),
                              child: SizedBox(
                                height: 34,
                                child: SearchBar(
                                  hintText: 'Поиск',
                                  hintStyle: MaterialStatePropertyAll(
                                      Theme.of(context)
                                          .textTheme
                                          .titleMedium
                                          ?.copyWith(
                                              fontWeight: FontWeight.w600,
                                              color: const Color.fromRGBO(
                                                  0, 0, 0, 0.5))),
                                  leading: SvgPicture.asset(
                                    'assets/icons/searchingIcon.svg',
                                    height: 20,
                                    width: 20,
                                    fit: BoxFit.scaleDown,
                                  ),
                                  padding: const MaterialStatePropertyAll(
                                      EdgeInsets.symmetric(
                                    horizontal: 6,
                                  )),
                                  onChanged: (value) {
                                    setState(() {
                                      searchString = value;
                                    });
                                  },
                                ),
                              ),
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 12),
                              child: Row(
                                children: [
                                  ///ВЫБОР ТЕМАТИКИ
                                  GestureDetector(
                                    onTap: () {
                                      if(snapshot.data!.subjectList.isNotEmpty) {
                                        bloc.action.add(
                                            ClearSelectedSubjects());
                                        bloc.action.add(
                                            ClearChooseSubjectList());
                                        subjectsPopup(
                                            context,
                                            snapshot.data!.subjectList,
                                            bloc.action);
                                      }
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Theme.of(context)
                                            .currentAppThemeColor,
                                        borderRadius:
                                            BorderRadius.circular(10),
                                      ),
                                      height: 32,
                                      width: MediaQuery.of(context).size.width/2,
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12, right: 12),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Тематика',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headlineSmall
                                                  ?.copyWith(
                                                      color: Colors.white),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 6),
                                              child: SvgPicture.asset(
                                                'assets/icons/chevron_down.svg',
                                                height: 20,
                                                width: 20,
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),

                                  const SizedBox(
                                    width: 12,
                                  ),
                                ],
                              ),
                            ),

                            ///ПЛИТКИ
                            Padding(
                              padding: const EdgeInsets.only(top: 12),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Wrap(
                                  spacing: 12,
                                  runSpacing: 12,
                                  children: [
                                    /// СПИСОК ТЕМ
                                    ...List.generate(
                                        snapshot.data!.selectedSubjects.length,
                                        (index) => Container(
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .currentAppThemeColor,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              height: 32,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 12, right: 12),
                                                child: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      snapshot
                                                          .data!
                                                          .selectedSubjects[index]
                                                          .title,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headlineSmall
                                                          ?.copyWith(
                                                              color:
                                                                  Colors.white),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 6),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          bloc.action.add(
                                                              DeleteSelectedSubject(
                                                                  index));
                                                        },
                                                        child: SvgPicture.asset(
                                                          'assets/icons/cancel.svg',
                                                          height: 20,
                                                          width: 20,
                                                          fit: BoxFit.scaleDown,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )),
                                  ],
                                ),
                              ),
                            ),

                            const SizedBox(
                              height: 8,
                            ),

                            ///СПИСОК СТАТЕЙ
                            ...List.generate(
                                snapshot.data!.articleWithSubjectsList.length,
                                (index) {
                              return Visibility(
                                visible: (snapshot
                                            .data!
                                            .articleWithSubjectsList[index]
                                            .title
                                            .contains(searchString)
                                    &&
                                    getSubjectsMatch(
                                            snapshot.data!
                                                .articleWithSubjectsList[index],
                                            snapshot)),
                                child: AvailableArticleForList(
                                  article: snapshot
                                      .data!.articleWithSubjectsList[index],
                                ),
                              );
                            }),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/addArticle');
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.add,
                              color: Colors.white,
                              opticalSize: 18,
                            ),
                            Text(
                              'Добавить статью',
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall
                                  ?.copyWith(color: Colors.white),
                            )
                          ],
                        )),
                  )
                ],
              );
            }
          }),
    );
  }
}
