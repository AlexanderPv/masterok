import 'dart:io';
import 'package:flutter/material.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/ui/form_fields/property_field.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/ui/form_fields/image_field.dart';
import 'package:master_ok_new/ui/form_fields/main_text_field_for_edit.dart';
import 'package:master_ok_new/models/article.dart';
import 'package:master_ok_new/ui/form_fields/subject_property_field.dart';

class EditArticle extends StatefulWidget {
  const EditArticle({super.key});

  @override
  State<EditArticle> createState() => _EditArticleState();
}

class _EditArticleState extends State<EditArticle> {
  final GlobalKey<FormState> newArticleFormKey = GlobalKey<FormState>();

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController subjectsController = TextEditingController();
  TextEditingController photosController = TextEditingController();
  TextEditingController videolinksController = TextEditingController();
  TextEditingController textController = TextEditingController();

  late final AppBloc bloc;
  late Article article;
  bool dataIsSet = false;
  bool articlesImagesIsSet = false;
  final List<File> images = [];

  void initControllers() {
    if (!dataIsSet) {
      titleController.text = article.title;
      descriptionController.text = article.description ?? '';
      videolinksController.text = article.videoLinks ?? '';
      textController.text = article.mainText ?? '';
      dataIsSet = true;
    }
  }

  @override
  void initState() {
    bloc = AppBloc();
    bloc.action.add(InitEditArticle());
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    titleController.dispose();
    descriptionController.dispose();
    subjectsController.dispose();
    photosController.dispose();
    videolinksController.dispose();
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    article = ModalRoute.of(context)!.settings.arguments as Article;
    initControllers();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Редактирование статьи'),
        centerTitle: true,
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              if (!articlesImagesIsSet) {
                if (snapshot.data!.currentArticleSubjectList.isNotEmpty) {
                  subjectsController.text =
                      'Выбрано тем: ${snapshot.data!.currentArticleSubjectList.length}';
                }
                if (snapshot.data!.fileFromImageList.isNotEmpty) {
                  for (var e in snapshot.data!.fileFromImageList) {
                    images.add(e);
                  }
                }
                articlesImagesIsSet = true;
              }
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 20, left: 20, right: 20, bottom: 20),
                          child: Form(
                            key: newArticleFormKey,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'О статье',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineSmall,
                                  ),
                                  PropertyField(
                                    fieldName: 'Название*',
                                    fieldController: titleController,
                                    hintText: 'Введите название',
                                    validate: true, forEdit: true,
                                  ),
                                  PropertyField(
                                    fieldName: 'Короткое описание',
                                    fieldController: descriptionController,
                                    hintText: 'Введите описание',
                                    validate: false, forEdit: true,
                                  ),
                                  SubjectPropertyField(
                                    fieldName: 'Тематика статьи',
                                    fieldController: subjectsController,
                                    hintText: 'Выберите тематику',
                                    validate: false, forEdit: true,
                                  ),


                                  /// СТАТЬЯ
                                  Padding(
                                    padding: const EdgeInsets.only(top: 34),
                                    child: Text(
                                      'Статья',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineSmall,
                                    ),
                                  ),

                                  ///ФОТОГРАФИИ
                                  ImageField(
                                    images: images,
                                  ),

                                  ///ССЫЛКА
                                  PropertyField(
                                    fieldName: 'Ссылка на видеоматериалы',
                                    fieldController: videolinksController,
                                    hintText: 'Вставьте ссылку',
                                    validate: false, forEdit: true,
                                  ),

                                  ///ТЕКСТ СТАТЬИ
                                  MainTextFieldForEdit(
                                      fieldName: 'Текст статьи',
                                      fieldController: textController,
                                      hintText: 'Введите текст',
                                      validate: false)
                                ]),
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: ElevatedButton(
                        onPressed: () {
                          if (newArticleFormKey.currentState!.validate()) {
                            Article newArticle = Article(
                              id: article.id,
                                title: titleController.text,
                                description: descriptionController.text,
                                videoLinks: videolinksController.text,
                                mainText: textController.text);
                            bloc.action.add(AddPicture(images));
                            bloc.action.add(UpdateArticle(newArticle));
                            Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.add,
                              color: Colors.white,
                              opticalSize: 18,
                            ),
                            Text(
                              'Готово',
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall
                                  ?.copyWith(color: Colors.white),
                            )
                          ],
                        )),
                  )
                ],
              );
            }
          }),
    );
  }
}
