import 'package:flutter/material.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:flutter_svg/svg.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/models/subject_model.dart';

class ChooseSubject extends StatefulWidget {
  const ChooseSubject({super.key, required this.fieldController});

  final TextEditingController fieldController;

  @override
  State<ChooseSubject> createState() => _ChooseSubjectState();
}

class _ChooseSubjectState extends State<ChooseSubject> {
  TextEditingController subjectNameController = TextEditingController();

  bool addSubjectVisibility = false;
  late final AppBloc bloc;
  String searchString = '';
  //late List<String> subjectList;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
    bloc.action.add(InitSubjectList());
  }

  @override
  void dispose() {
    subjectNameController.dispose();
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppState>(
        stream: bloc.state,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.only(top: 28),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Row(
                            children: [
                              Icon(Icons.arrow_back_ios,
                                  color:
                                      Theme.of(context).currentAppThemeColor),
                              Text(
                                'Отмена',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall
                                    ?.copyWith(
                                        color: Theme.of(context)
                                            .currentAppThemeColor),
                              ),
                            ],
                          ),
                        ),
                        Text(
                          'Выберите тематику',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        GestureDetector(
                          onTap: () {
                            int x = 0;
                            for (int i = 0;
                                i < snapshot.data!.chooseSubjectList.length;
                                i++) {
                              if (snapshot.data!.chooseSubjectList[i]) {
                                x++;
                              }
                              if (x > 0) {
                                widget.fieldController.text =
                                    'Выбрано тем: $x';
                              } else {
                                widget.fieldController.text = '';
                              }
                            }
                            Navigator.pop(context);
                          },
                          child: Text(
                            'Готово',
                            style: Theme.of(context)
                                .textTheme
                                .headlineSmall
                                ?.copyWith(
                                    color:
                                        Theme.of(context).currentAppThemeColor),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 12, left: 20, right: 20),
                    child: SizedBox(
                      height: 34,
                      child: SearchBar(
                        hintText: 'Поиск',
                        hintStyle: MaterialStatePropertyAll(Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(
                                fontWeight: FontWeight.w600,
                                color: const Color.fromRGBO(0, 0, 0, 0.5))),
                        leading:
                            SvgPicture.asset(
                          'assets/icons/searchingIcon.svg',
                          height: 20,
                          width: 20,
                          fit: BoxFit.scaleDown,
                        ),
                        padding:
                            const MaterialStatePropertyAll(EdgeInsets.symmetric(
                          horizontal: 6,
                        )),
                        onChanged: (value) {
                          setState(() {
                            searchString = value;
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Column(
                          children: [
                            ///СПИСОК ТЕМАТИК
                            Padding(
                              padding: const EdgeInsets.only(top: 14),
                              child: Column(
                                children: [
                                  ...List.generate(
                                      snapshot.data!.subjectList.length, (index) {
                                    return Visibility(
                                      visible: snapshot
                                              .data!.subjectList[index].title
                                              .contains(searchString)
                                          ? true
                                          : false,
                                      child: Dismissible(
                                        direction: DismissDirection.endToStart,
                                        key: UniqueKey(),
                                        background: Container(
                                          color: const Color.fromRGBO(
                                              255, 59, 48, 1),
                                        ),
                                        secondaryBackground: Container(
                                          color: const Color.fromRGBO(
                                              255, 59, 48, 1),
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 8),
                                              child: SvgPicture.asset(
                                                'assets/icons/trashIcon.svg',
                                                colorFilter:
                                                    const ColorFilter.mode(
                                                        Colors.white,
                                                        BlendMode.srcIn),
                                                height: 28,
                                                width: 28,
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                          ),
                                        ),
                                        onDismissed:
                                            (DismissDirection direction) {
                                          bloc.action.add(DeleteSubject(
                                              snapshot.data!.subjectList[index],
                                              index));
                                        },
                                        child: Container(
                                          height: 48,
                                          width: double.infinity,
                                          decoration: const BoxDecoration(
                                              border: Border(
                                                  top: BorderSide(
                                                      color: Color.fromRGBO(
                                                          0, 0, 0, 0.3),
                                                      width: 0.5))),
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(left: 8),
                                            child: Row(
                                              children: [
                                                Checkbox(
                                                    side: const BorderSide(
                                                        width: 1,
                                                        color: Color.fromRGBO(
                                                            0, 0, 0, 0.5)),
                                                    activeColor: Theme.of(
                                                            context)
                                                        .currentAppThemeColor,
                                                    value: snapshot.data
                                                            ?.chooseSubjectList[
                                                        index],
                                                    onChanged: (bool? value) {
                                                      bloc.action.add(
                                                          SetChooseSubjectListItem(
                                                              index, value!));
                                                    }),
                                                Text(
                                                  snapshot.data!
                                                      .subjectList[index].title,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .titleMedium,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  })
                                ],
                              ),
                            ),
                            //   ],
                            // ),

                            /// НАЗВАНИЕ НОВОЙ ТЕМАТИКИ
                            Visibility(
                              visible: addSubjectVisibility,
                              child: Container(
                                decoration: const BoxDecoration(
                                    border: Border(
                                        top: BorderSide(
                                            color: Color.fromRGBO(0, 0, 0, 0.3),
                                            width: 0.5))),
                                child: TextField(
                                  textInputAction: TextInputAction.done,
                                  onSubmitted: (value) {
                                    if (subjectNameController.text != '') {
                                      Subject newSubject =
                                          Subject(title: subjectNameController.text);
                                      bloc.action.add(AddNewSubject(newSubject));
                                    }
                                    setState(() {
                                      addSubjectVisibility = !addSubjectVisibility;
                                    });
                                    subjectNameController.text = '';
                                  },
                                  style:
                                      Theme.of(context).textTheme.titleMedium,
                                  controller: subjectNameController,
                                  maxLength: 100,
                                  decoration: InputDecoration(
                                      prefixIcon: Padding(
                                        padding: const EdgeInsets.only(left: 8),
                                        child: Checkbox(
                                            side: const BorderSide(
                                                width: 1,
                                                color: Color.fromRGBO(
                                                    0, 0, 0, 0.5)),
                                            value: false,
                                            onChanged: (bool? value) {}),
                                      ),
                                      counterText: '',
                                      counterStyle: null,
                                      hintText: 'Введите название темы',
                                      hintStyle: Theme.of(context)
                                          .textTheme
                                          .titleMedium
                                          ?.copyWith(
                                              color: const Color.fromRGBO(
                                                  0, 0, 0, 0.6)),
                                      border: InputBorder.none,
                                      contentPadding: const EdgeInsets.all(12)),
                                ),
                              ),
                            ),

                            /// ДОБАВИТЬ ТЕМУ
                            GestureDetector(
                              onTap: () {
                                if (!addSubjectVisibility) {
                                  setState(() {
                                    addSubjectVisibility = !addSubjectVisibility;
                                  });
                                } else {
                                  if (subjectNameController.text != '') {
                                    Subject newSubject =
                                    Subject(title: subjectNameController.text);
                                    bloc.action.add(AddNewSubject(newSubject));
                                  }
                                  setState(() {
                                    addSubjectVisibility = !addSubjectVisibility;
                                  });
                                  subjectNameController.text = '';
                                }
                              },
                              child: Container(
                                height: 48,
                                width: double.infinity,
                                decoration: const BoxDecoration(
                                    border: Border(
                                  top: BorderSide(
                                      color: Color.fromRGBO(0, 0, 0, 0.3),
                                      width: 0.5),
                                  bottom: BorderSide(
                                      color: Color.fromRGBO(0, 0, 0, 0.3),
                                      width: 0.5),
                                )),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.add,
                                        color: Theme.of(context)
                                            .currentAppThemeColor,
                                        size: 24,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 16),
                                        child: Text(
                                          'Добавить тематику',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleMedium,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        });
  }
}
