import 'package:flutter/material.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/models/subject_model.dart';

Future<void> subjectsPopup(
    BuildContext context, List<Subject> subjectsList, Sink<ActionEvent> act) {
  List<bool> chooseList = List.generate(subjectsList.length, (index) => false);
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
            children: List.generate(subjectsList.length, (index) {
          return Container(
            height: 48,
            decoration: index != 0
                ? const BoxDecoration(
                    border: Border(
                        top: BorderSide(
                            color: Color.fromRGBO(0, 0, 0, 0.3), width: 0.5)))
                : null,
            child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Row(
                children: [
                  StatefulBuilder(
                    builder: (context, setState) {
                      return Checkbox(
                          side: const BorderSide(
                              width: 1, color: Color.fromRGBO(0, 0, 0, 0.5)),
                          activeColor: Theme.of(context).currentAppThemeColor,
                          value: chooseList[index],
                          onChanged: (bool? value) {
                            setState(() {
                              chooseList[index] = value!;
                            });
                            act.add(SetChooseSubjectListItem(index, value!));
                            act.add(GetSelectedSubjects());
                          });
                    },
                  ),
                  Text(
                    subjectsList[index].title,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
            ),
          );
        }));
      });
}
