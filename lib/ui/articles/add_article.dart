import 'dart:io';
import 'package:flutter/material.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/ui/form_fields/property_field.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/ui/form_fields/image_field.dart';
import 'package:master_ok_new/ui/form_fields/main_text_field.dart';
import 'package:master_ok_new/ui/form_fields/subject_property_field.dart';
import 'package:master_ok_new/models/article.dart';


class AddArticle extends StatefulWidget {
  const AddArticle({super.key});

  @override
  State<AddArticle> createState() => _AddArticleState();
}

class _AddArticleState extends State<AddArticle> {
  final GlobalKey<FormState> newArticleFormKey = GlobalKey<FormState>();

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController subjectsController = TextEditingController();
  TextEditingController photosController = TextEditingController();
  TextEditingController videolinksController = TextEditingController();
  TextEditingController textController = TextEditingController();

  late final AppBloc bloc;

  final List <File> images = [];

  @override
  void initState() {
    bloc = AppBloc();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    titleController.dispose();
    descriptionController.dispose();
    subjectsController.dispose();
    photosController.dispose();
    videolinksController.dispose();
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Добавление статьи'),
        centerTitle: true,
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                        padding: const EdgeInsets.only(
                            top: 20, left: 20, right: 20, bottom: 20),
                        child: Form(
                          key: newArticleFormKey,
                          child: Column(
                              crossAxisAlignment:
                                  CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Об инструкции',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall,
                                ),
                                PropertyField(
                                  fieldName: 'Название*',
                                  fieldController: titleController,
                                  hintText: 'Введите название',
                                  validate: true, forEdit: false,
                                ),
                                PropertyField(
                                  fieldName: 'Короткое описание',
                                  fieldController: descriptionController,
                                  hintText: 'Введите описание',
                                  validate: false, forEdit: false,
                                ),
                                SubjectPropertyField(
                                  fieldName: 'Тематика статьи',
                                  fieldController: subjectsController,
                                  hintText: 'Выберите тематику',
                                  validate: false, forEdit: false,
                                ),

                                ///СТАТЬЯ
                                Padding(
                                  padding: const EdgeInsets.only(top: 34),
                                  child: Text(
                                    'Статья',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineSmall,
                                  ),
                                ),

                                ///ФОТОГРАФИИ
                                ImageField(images: images,),
                                ///ССЫЛКА
                                PropertyField(
                                  fieldName: 'Ссылка на видеоматериалы',
                                  fieldController: videolinksController,
                                  hintText: 'Вставьте ссылку',
                                  validate: false, forEdit: false,
                                ),
                                ///ТЕКСТ СТАТЬИ
                                MainTextField(
                                    fieldName: 'Текст статьи',
                                    fieldController: textController,
                                    hintText: 'Введите текст',
                                    validate: false)
                              ]),
                        )),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(12),
                  child: ElevatedButton(
                      onPressed: () {
                        if (
                        newArticleFormKey.currentState!.validate()) {
                          Article article = Article(
                            title: titleController.text,
                            description: descriptionController.text,
                            videoLinks: videolinksController.text,
                            mainText: textController.text
                          );
                          bloc.action.add(AddPicture(images));
                          bloc.action.add(AddNewArticle(article));
                          Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.add,
                            color: Colors.white,
                            opticalSize: 18,
                          ),
                          Text(
                            'Добавить статью',
                            style: Theme.of(context)
                                .textTheme
                                .headlineSmall
                                ?.copyWith(color: Colors.white),
                          )
                        ],
                      )),
                )
              ],
            );
          }),
    );
  }
}
