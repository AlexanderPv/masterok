import 'package:flutter/material.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/models/manual.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/models/manual.dart';

class ManualPopUpMenu extends StatefulWidget {
  const ManualPopUpMenu({super.key, required this.width, required this.manual});

  final Manual manual;
  final double width;

  @override
  State<ManualPopUpMenu> createState() => _ManualPopUpMenuState();
}

class _ManualPopUpMenuState extends State<ManualPopUpMenu> {
  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppState>(
        stream: bloc.state,
        builder: (context, snapshot) {
          return PopupMenuButton<int>(
            constraints: BoxConstraints(minWidth: widget.width * 0.64),
            icon: const Icon(
              Icons.more_horiz,
              size: 28,
            ),
            itemBuilder: (BuildContext context) => [
              PopupMenuItem<int>(
                value: 0,
                child: const Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text('Редактировать'),
                ),
                onTap: () {
                  bloc.action.add(InitManualInfo(widget.manual.id!));
                  Navigator.of(context)
                      .pushNamed('/editManual', arguments: widget.manual);
                },
              ),
              const PopupMenuDivider(),
              PopupMenuItem<int>(
                value: 1,
                child: const Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text('Удалить',
                      style: TextStyle(
                          color: Color.fromRGBO(255, 59, 48, 1),
                          fontWeight: FontWeight.w700)),
                ),
                onTap: () {
                  showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (context) {
                      return Dialog(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 20),
                              child: Column(
                                children: [
                                  Text(
                                    'Вы действительно хотите',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                  Text(
                                    'удалить инструкцию?',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      bloc.action
                                          .add(DeleteManual(widget.manual));
                                      Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                                      ('/main');
                                    },
                                    child: Container(
                                      height: 42,
                                      decoration: const BoxDecoration(
                                          border: Border(
                                              top: BorderSide(
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.3)),
                                              right: BorderSide(
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.3)))),
                                      child: Center(
                                        child: Text(
                                          'Удалить',
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineSmall
                                              ?.copyWith(
                                                  color: const Color.fromRGBO(
                                                      255, 59, 48, 1)),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Container(
                                      height: 42,
                                      decoration: const BoxDecoration(
                                          border: Border(
                                              top: BorderSide(
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.3)))),
                                      child: Center(
                                        child: Text(
                                          'Отмена',
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleMedium,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  );
                },
              ),
            ],
          );
        });
  }
}
