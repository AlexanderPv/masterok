import 'package:flutter/material.dart';
import 'package:master_ok_new/models/picture.dart';
import 'package:master_ok_new/themes/app_theme.dart';

class ImagePopup extends StatefulWidget {
  const ImagePopup(
      {super.key, required this.pictureList, required this.currentIndex});

  final List<Picture> pictureList;
  final int currentIndex;

  @override
  State<ImagePopup> createState() => _ImagePopupState();
}

class _ImagePopupState extends State<ImagePopup> {
  late int pictureIndex;

  @override
  void initState() {
    super.initState();
    pictureIndex = widget.currentIndex;
  }

  void incrementPictureIndex() {
    if (pictureIndex + 1 < widget.pictureList.length) {
      pictureIndex++;
    } else {
      pictureIndex = 0;
    }
  }

  void decrementPictureIndex() {
    if (pictureIndex > 0) {
      pictureIndex--;
    } else {
      pictureIndex = widget.pictureList.length - 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.close,
                  color: Colors.black,
                  size: 28,
                )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    decrementPictureIndex();
                  });
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Theme.of(context).currentAppThemeColor,
                ),
              ),
              Image.memory(
                widget.pictureList[pictureIndex].picture,
                height: MediaQuery.of(context).size.height * 0.85 - 94,
                width: MediaQuery.of(context).size.width - 80,
                fit: BoxFit.fitWidth,
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    incrementPictureIndex();
                  });
                },
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Theme.of(context).currentAppThemeColor,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 28,
          )
        ],
      ),
    );
  }
}
