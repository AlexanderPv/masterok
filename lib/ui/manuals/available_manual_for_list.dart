import 'package:flutter/material.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/models/manual.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/models/manual_with_tools.dart';


class AvailableManualForList extends StatefulWidget {
  const AvailableManualForList({super.key, required this.manual});

  final ManualWithTools manual;

  @override
  State<AvailableManualForList> createState() => _AvailableManualForListState();
}

class _AvailableManualForListState extends State<AvailableManualForList> {

  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppState>(
      stream: bloc.state,
      builder: (context, snapshot) {
        return GestureDetector(
          onTap: () {
            bloc.action.add(InitManualInfo(widget.manual.id!));
            Navigator.of(context).pushNamed('/aboutManual', arguments: widget.manual);
          },
          child: Container(
            margin: const EdgeInsets.only(top: 12),
            height: 66,
            width: double.infinity,
            padding: const EdgeInsets.all(12),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.25),
                      offset: Offset(10, 10),
                      blurRadius: 35,
                      spreadRadius: -15,
                      blurStyle: BlurStyle.normal)
                ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.manual.title,
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.headlineSmall,
                  overflow: TextOverflow.ellipsis,
                ),
                Row(
                  children: [
                    Text(
                      'Сложность:',
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: const Color.fromRGBO(0, 0, 0, 0.5)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12),
                      child: Row(
                        children: List.generate(
                            widget.manual.complexity != null ? widget.manual.complexity! + 1 : 0,
                                (index) => const Padding(
                              padding: EdgeInsets.only(right: 4),
                              child: Icon(
                                Icons.circle,
                                color: Color.fromRGBO(0, 0, 0, 0.5),
                                size: 8,
                              ),
                            )),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      }
    );
  }
}

