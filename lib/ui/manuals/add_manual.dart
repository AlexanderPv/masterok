import 'dart:io';
import 'package:flutter/material.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/ui/form_fields/property_field.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/ui/form_fields/tools_property_field.dart';
import 'package:master_ok_new/ui/form_fields/complexity_property_field.dart';
import 'package:master_ok_new/ui/form_fields/image_field.dart';
import 'package:master_ok_new/ui/form_fields/main_text_field.dart';
import 'package:master_ok_new/models/manual.dart';


class AddManual extends StatefulWidget {
  const AddManual({super.key});

  @override
  State<AddManual> createState() => _AddManualState();
}

class _AddManualState extends State<AddManual> {
  final GlobalKey<FormState> newManualFormKey = GlobalKey<FormState>();

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController toolsController = TextEditingController();
  TextEditingController complexityController = TextEditingController();
  TextEditingController photosController = TextEditingController();
  TextEditingController videolinksController = TextEditingController();
  TextEditingController textController = TextEditingController();

  late final AppBloc bloc;

  final List <File> images = [];

  @override
  void initState() {
    bloc = AppBloc();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    titleController.dispose();
    descriptionController.dispose();
    toolsController.dispose();
    complexityController.dispose();
    photosController.dispose();
    videolinksController.dispose();
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Добавление инструкции'),
        centerTitle: true,
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                        padding: const EdgeInsets.only(
                            top: 20, left: 20, right: 20, bottom: 20),
                        child: Form(
                          key: newManualFormKey,
                          child: Column(
                              crossAxisAlignment:
                                  CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Об инструкции',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall,
                                ),
                                PropertyField(
                                  fieldName: 'Название*',
                                  fieldController: titleController,
                                  hintText: 'Введите название',
                                  validate: true, forEdit: false,
                                ),
                                PropertyField(
                                  fieldName: 'Короткое описание',
                                  fieldController: descriptionController,
                                  hintText: 'Введите описание',
                                  validate: false, forEdit: false,
                                ),
                                ToolsPropertyField(
                                  fieldName: 'Необходимые инструменты',
                                  fieldController: toolsController,
                                  hintText: 'Выберите инструменты',
                                  validate: false, forEdit: false,
                                ),

                                ///СЛОЖНОСТЬ
                                ComplexPropertyField(
                                  fieldName: 'Выберите сложность',
                                  fieldController: complexityController,
                                ),

                                ///ИНСТРУКЦИЯ
                                Padding(
                                  padding: const EdgeInsets.only(top: 34),
                                  child: Text(
                                    'Инструкция',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineSmall,
                                  ),
                                ),

                                ///ФОТОГРАФИИ
                                ImageField(images: images,),
                                ///ССЫЛКА
                                PropertyField(
                                  fieldName: 'Ссылка на видеоматериалы',
                                  fieldController: videolinksController,
                                  hintText: 'Вставьте ссылку',
                                  validate: false, forEdit: false,
                                ),
                                ///ТЕКСТ ИНСТРУКЦИИ
                                MainTextField(
                                    fieldName: 'Текст инструкции',
                                    fieldController: textController,
                                    hintText: 'Введите текст',
                                    validate: false)
                              ]),
                        )),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(12),
                  child: ElevatedButton(
                      onPressed: () {
                        if (
                        newManualFormKey.currentState!.validate()) {
                          Manual manual = Manual(
                            title: titleController.text,
                            description: descriptionController.text,
                            complexity: int.tryParse(complexityController.text),
                            videoLinks: videolinksController.text,
                            mainText: textController.text
                          );
                          bloc.action.add(AddPicture(images));
                          bloc.action.add(AddNewManual(manual));
                          Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.add,
                            color: Colors.white,
                            opticalSize: 18,
                          ),
                          Text(
                            'Добавить инструкцию',
                            style: Theme.of(context)
                                .textTheme
                                .headlineSmall
                                ?.copyWith(color: Colors.white),
                          )
                        ],
                      )),
                )
              ],
            );
          }),
    );
  }
}
