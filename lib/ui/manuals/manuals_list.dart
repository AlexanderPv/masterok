import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/models/manual_with_tools.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/ui/main_screen/available_manual.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:master_ok_new/ui/manuals/tools_popup.dart';
import 'package:master_ok_new/models/toolModel.dart';
import 'package:master_ok_new/ui/manuals/available_manual_for_list.dart';

class ManualsList extends StatefulWidget {
  const ManualsList({super.key});

  @override
  State<ManualsList> createState() => _ManualsListState();
}

class _ManualsListState extends State<ManualsList> {
  late final AppBloc bloc;
  String searchString = '';

  int selectedComplexity = 0;

  bool getToolsMatch(
      ManualWithTools manualWithTools, AsyncSnapshot<AppState> snapshot) {
    bool match = true;
    if (snapshot.data!.selectedTools.isEmpty) {
      return true;
    } else if (manualWithTools.manualTools!.isEmpty) {
      return false;
    } else {
      for (var e in snapshot.data!.selectedTools) {
        bool x = false;
        for (var o in manualWithTools.manualTools!) {
          if (e.id == o.toolId) {
            x = x || true;
          }
        }
        match = match && x;
      }
    }
    return match;
  }

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
    bloc.action.add(InitManualsList());
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Инструкции'),
        centerTitle: true,
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                              ),
                              child: SizedBox(
                                height: 34,
                                child: SearchBar(
                                  hintText: 'Поиск',
                                  hintStyle: MaterialStatePropertyAll(
                                      Theme.of(context)
                                          .textTheme
                                          .titleMedium
                                          ?.copyWith(
                                              fontWeight: FontWeight.w600,
                                              color: const Color.fromRGBO(
                                                  0, 0, 0, 0.5))),
                                  leading: SvgPicture.asset(
                                    'assets/icons/searchingIcon.svg',
                                    height: 20,
                                    width: 20,
                                    fit: BoxFit.scaleDown,
                                  ),
                                  padding: const MaterialStatePropertyAll(
                                      EdgeInsets.symmetric(
                                    horizontal: 6,
                                  )),
                                  onChanged: (value) {
                                    setState(() {
                                      searchString = value;
                                    });
                                  },
                                ),
                              ),
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 12),
                              child: Row(
                                children: [
                                  ///ВЫБОР ИНСТРУМЕНТОВ
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        if(snapshot.data!.toolsList.isNotEmpty) {
                                          bloc.action.add(ClearSelectedTools());
                                          bloc.action.add(
                                              ClearChooseToolsList());
                                          toolsPopup(
                                              context,
                                              snapshot.data!.toolsList,
                                              bloc.action);
                                        }
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Theme.of(context)
                                              .currentAppThemeColor,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        height: 32,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 12, right: 12),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                'Инструменты',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headlineSmall
                                                    ?.copyWith(
                                                        color: Colors.white),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 6),
                                                child: SvgPicture.asset(
                                                  'assets/icons/chevron_down.svg',
                                                  height: 20,
                                                  width: 20,
                                                  fit: BoxFit.scaleDown,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                  const SizedBox(
                                    width: 12,
                                  ),

                                  ///ВЫБОР СЛОЖНОСТИ

                                  GestureDetector(
                                    onTap: () {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return SimpleDialog(
                                                children:
                                                    List.generate(5, (index) {
                                              return GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    selectedComplexity =
                                                        5 - index;
                                                  });
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  height: 48,
                                                  decoration: index != 0
                                                      ? const BoxDecoration(
                                                          border: Border(
                                                              top: BorderSide(
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          0.3),
                                                                  width: 0.5)))
                                                      : null,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 22),
                                                    child: Row(
                                                      children: List.generate(
                                                          5 - index,
                                                          (index2) =>
                                                              const Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        right:
                                                                            14),
                                                                child: Icon(
                                                                  Icons.circle,
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          0.5),
                                                                  size: 12,
                                                                ),
                                                              )),
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }));
                                          });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Theme.of(context)
                                            .currentAppThemeColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      height: 32,
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12, right: 12),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Сложность',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headlineSmall
                                                  ?.copyWith(
                                                      color: Colors.white),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 6),
                                              child: SvgPicture.asset(
                                                'assets/icons/chevron_down.svg',
                                                height: 20,
                                                width: 20,
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),

                            ///ПЛИТКИ
                            Padding(
                              padding: const EdgeInsets.only(top: 12),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Wrap(
                                  spacing: 12,
                                  runSpacing: 12,
                                  children: [
                                    /// СПИСОК ИНСТРУМЕНТОВ
                                    ...List.generate(
                                        snapshot.data!.selectedTools.length,
                                        (index) => Container(
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .currentAppThemeColor,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              height: 32,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 12, right: 12),
                                                child: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      snapshot
                                                          .data!
                                                          .selectedTools[index]
                                                          .title,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headlineSmall
                                                          ?.copyWith(
                                                              color:
                                                                  Colors.white),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 6),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          bloc.action.add(
                                                              DeleteSelectedTool(
                                                                  index));
                                                        },
                                                        child: SvgPicture.asset(
                                                          'assets/icons/cancel.svg',
                                                          height: 20,
                                                          width: 20,
                                                          fit: BoxFit.scaleDown,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )),

                                    /// СЛОЖНОСТЬ
                                    selectedComplexity != 0
                                        ? Container(
                                            decoration: BoxDecoration(
                                              color: Theme.of(context)
                                                  .currentAppThemeColor,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            height: 32,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 12, right: 12),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  ...List.generate(
                                                      selectedComplexity,
                                                      (index2) => const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    right: 4),
                                                            child: Icon(
                                                              Icons.circle,
                                                              color:
                                                                  Colors.white,
                                                              size: 8,
                                                            ),
                                                          )),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 6),
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          selectedComplexity =
                                                              0;
                                                        });
                                                      },
                                                      child: SvgPicture.asset(
                                                        'assets/icons/cancel.svg',
                                                        height: 20,
                                                        width: 20,
                                                        fit: BoxFit.scaleDown,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        : const SizedBox.shrink(),
                                  ],
                                ),
                              ),
                            ),

                            const SizedBox(
                              height: 8,
                            ),

                            ///СПИСОК ИНСТРУКЦИЙ
                            ...List.generate(
                                snapshot.data!.manualWithToolsList.length,
                                (index) {
                              return Visibility(
                                visible:
                                (snapshot.data!
                                            .manualWithToolsList[index].title
                                            .contains(searchString)

                                    &&
                                        getToolsMatch(
                                            snapshot.data!
                                                .manualWithToolsList[index],
                                            snapshot))
                                    &&
                                    (selectedComplexity != 0
                                    ? snapshot.data!.manualWithToolsList[index]
                                    .complexity ==
                                    selectedComplexity - 1
                                    : true)
                                ,
                                child: AvailableManualForList(
                                  manual:
                                      snapshot.data!.manualWithToolsList[index],
                                ),
                              );
                            }),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/addManual');
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.add,
                              color: Colors.white,
                              opticalSize: 18,
                            ),
                            Text(
                              'Добавить инструкцию',
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall
                                  ?.copyWith(color: Colors.white),
                            )
                          ],
                        )),
                  )
                ],
              );
            }
          }),
    );
  }
}
