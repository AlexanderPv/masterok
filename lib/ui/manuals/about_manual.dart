import 'package:flutter/material.dart';
import 'package:master_ok_new/data/utils.dart';
import 'package:master_ok_new/models/manual.dart';
import 'package:master_ok_new/ui/manuals/manual_popup_menu.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/ui/manuals/image_popup.dart';
import 'package:master_ok_new/themes/app_theme.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutManual extends StatefulWidget {
  const AboutManual({super.key});

  @override
  State<AboutManual> createState() => _AboutManualState();
}

class _AboutManualState extends State<AboutManual> {
  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
    bloc.action.add(GetStreamForManualInfo());
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Manual manual = ModalRoute.of(context)!.settings.arguments as Manual;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Инструкция'),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 12),
            child: ManualPopUpMenu(
              width: MediaQuery.of(context).size.width,
              manual: manual,
            ),
          ),
        ],
      ),
      body: StreamBuilder<AppState>(
          stream: bloc.state,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Об инструкции',
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Название',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          manual.title,
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Text(
                          'Короткое описание',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          manual.description ?? '',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Text(
                          'Необходимые инструменты',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(top: 4),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: List.generate(
                                  snapshot.data!.currentManualToolsList.length,
                                  (index) =>
                                      // Text(index.toString())
                                      Text(
                                          '  ${index + 1}. ${snapshot.data!.currentManualToolsList[index].toolTitle}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleMedium)))),
                      Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Сложность',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 2),
                              child: Row(
                                children: List.generate(
                                    manual.complexity != null
                                        ? manual.complexity! + 1
                                        : 0,
                                    (index) => const Padding(
                                          padding: EdgeInsets.only(right: 14),
                                          child: Icon(
                                            Icons.circle,
                                            color: Color.fromRGBO(0, 0, 0, 0.5),
                                            size: 12,
                                          ),
                                        )),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Text(
                          'Инструкция',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Фотографии',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Row(
                          children: List.generate(
                              snapshot.data!.currentPictureModelsList.length,
                              (index) => Padding(
                                    padding: const EdgeInsets.only(right: 12),
                                    child: GestureDetector(
                                      onTap: () {
                                        showModalBottomSheet(
                                            clipBehavior: Clip.antiAlias,
                                            context: context,
                                            isScrollControlled: true,
                                            isDismissible: false,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(12)),
                                            builder: (BuildContext context) {
                                              return DraggableScrollableSheet(
                                                initialChildSize: 0.85,
                                                expand: false,
                                                builder: (BuildContext context,
                                                    ScrollController
                                                        scrollController) {
                                                  return ImagePopup(
                                                    pictureList: snapshot.data!
                                                        .currentPictureModelsList,
                                                    currentIndex: index,
                                                  );
                                                },
                                              );
                                            });
                                      },
                                      child: Container(
                                        clipBehavior: Clip.antiAlias,
                                        decoration: BoxDecoration(
                                          color:
                                              const Color.fromRGBO(0, 0, 0, 0.05),
                                          borderRadius: BorderRadius.circular(12),
                                        ),
                                        height: 60,
                                        width: 60,
                                        child: Image.memory(
                                          snapshot
                                              .data!
                                              .currentPictureModelsList[index]
                                              .picture,
                                          width: 60,
                                          height: 60,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Ссылка на видеоматериалы',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: GestureDetector(
                          onTap: () async {
                            if (manual.videoLinks != null) {
                              final url = Uri.parse(
                                manual.videoLinks!,
                              );
                              if (await canLaunchUrl(url)) {
                                launchUrl(url);
                              }
                            }
                          },
                          child: Text(
                            manual.videoLinks ?? '',
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    color:
                                        Theme.of(context).currentAppThemeColor),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Текст инструкции',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          manual.mainText ?? '',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          }),
    );
  }
}
