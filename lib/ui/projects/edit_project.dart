import 'package:flutter/material.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/ui/form_fields/property_field.dart';
import 'package:master_ok_new/ui/form_fields/digital_property_field.dart';
import 'package:master_ok_new/ui/form_fields/date_property_field.dart';
import 'package:master_ok_new/ui/form_fields/phone_property_field.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/actions/actions.dart';

class EditProject extends StatefulWidget {
  const EditProject({super.key});

  @override
  State<EditProject> createState() => _EditProjectState();
}

class _EditProjectState extends State<EditProject> {
  late final AppBloc bloc;

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController budgetController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController clientController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  // final titleFieldKey = GlobalKey<FormFieldState>();
  // final descriptionFieldKey = GlobalKey<FormFieldState>();
  // final budgetFieldKey = GlobalKey<FormFieldState>();
  // final dateFieldKey = GlobalKey<FormFieldState>();
  // final clientFieldKey = GlobalKey<FormFieldState>();
  // final phoneFieldKey = GlobalKey<FormFieldState>();

  final GlobalKey<FormState> newProjectFormKey = GlobalKey<FormState>();

  late Project project;
  bool dataIsSet = false;

  void initControllers() {
    if (!dataIsSet) {
      dateController.text = project.date ?? '';
      titleController.text = project.title;
      descriptionController.text = project.description;
      budgetController.text =
          project.budget == null ? '' : project.budget.toString();
      clientController.text = project.client ?? '';
      phoneController.text = project.phone ?? '';
      dataIsSet = true;
    }
  }

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
  }

  @override
  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    budgetController.dispose();
    dateController.dispose();
    clientController.dispose();
    phoneController.dispose();
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    project = ModalRoute.of(context)!.settings.arguments as Project;
    initControllers();
    return StreamBuilder<AppState>(
        stream: bloc.state,
        builder: (context, snapshot) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Редактирование проекта'),
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 20, left: 20, right: 20, bottom: 70),
                      child: Form(
                        key: newProjectFormKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Отредактируйте информацию',
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 12),
                              child: Text(
                                'О проекте',
                                style:
                                    Theme.of(context).textTheme.headlineSmall,
                              ),
                            ),
                            PropertyField(
                              fieldName: 'Название*',
                              fieldController: titleController,
                              // fieldKey: titleFieldKey,
                              hintText: 'Введите название',
                              validate: true, forEdit: true,
                            ),
                            PropertyField(
                              fieldName: 'Описание',
                              fieldController: descriptionController,
                              // fieldKey: descriptionFieldKey,
                              hintText: 'Введите описание',
                              validate: false, forEdit: true,
                            ),
                            DigitalPropertyField(
                              fieldName: 'Бюджет',
                              fieldController: budgetController,
                              // fieldKey: budgetFieldKey,
                              hintText: 'Введите сумму в рублях',
                              validate: false,
                            ),
                            DatePropertyField(
                              fieldName: 'Сроки выполнения',
                              fieldController: dateController,
                              // fieldKey: dateFieldKey,
                              hintText: 'Выберите дату',
                              validate: false,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Text(
                                'О заказчике',
                                style:
                                    Theme.of(context).textTheme.headlineSmall,
                              ),
                            ),
                            PropertyField(
                              fieldName: 'Имя заказчика',
                              fieldController: clientController,
                              // fieldKey: clientFieldKey,
                              hintText: 'Введите имя',
                              validate: false, forEdit: true,
                            ),
                            PhonePropertyField(
                              fieldName: 'Контактный номер телефона',
                              fieldController: phoneController,
                              // fieldKey: phoneFieldKey,
                              hintText: 'Введите телефон',
                              validate: false,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                        onPressed: () {
                          if (newProjectFormKey.currentState!.validate()) {
                            Project newProject = Project(
                              id: project.id,
                              title: titleController.text,
                              description: descriptionController.text,
                              budget: int.tryParse(budgetController.text),
                              date: dateController.text,
                              client: clientController.text,
                              phone: phoneController.text,
                            );
                            bloc.action.add(UpdateProject(newProject));
                            Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                          }
                        },
                        child: Text(
                          'Готово',
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall
                              ?.copyWith(color: Colors.white),
                        )),
                  ),
                )
              ],
            ),
          );
        });
  }
}
