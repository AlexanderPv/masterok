import 'package:flutter/material.dart';
import 'package:master_ok_new/ui/form_fields/property_field.dart';
import 'package:master_ok_new/ui/form_fields/digital_property_field.dart';
import 'package:master_ok_new/ui/form_fields/date_property_field.dart';
import 'package:master_ok_new/ui/form_fields/phone_property_field.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/actions/actions.dart';

class AddProject extends StatefulWidget {
  const AddProject({super.key});


  @override
  State<AddProject> createState() => _AddProjectState();
}

class _AddProjectState extends State<AddProject> {

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController budgetController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController clientController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  // final titleFieldKey = GlobalKey<FormFieldState>();
  // final descriptionFieldKey = GlobalKey<FormFieldState>();
  // final budgetFieldKey = GlobalKey<FormFieldState>();
  // final dateFieldKey = GlobalKey<FormFieldState>();
  // final clientFieldKey = GlobalKey<FormFieldState>();
  // final phoneFieldKey = GlobalKey<FormFieldState>();

  final GlobalKey<FormState> newProjectFormKey = GlobalKey<FormState>();

  @override
  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    budgetController.dispose();
    dateController.dispose();
    clientController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppBloc bloc = ModalRoute.of(context)!.settings.arguments as AppBloc;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Добавление проекта'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 20, right: 20, bottom: 20),
                child: Form(
                  key: newProjectFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'О проекте',
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                      PropertyField(
                        fieldName: 'Название*',
                        fieldController: titleController,
                        // fieldKey: titleFieldKey,
                        hintText: 'Введите название',
                        validate: true, forEdit: false,
                      ),
                      PropertyField(
                        fieldName: 'Описание',
                        fieldController: descriptionController,
                        // fieldKey: descriptionFieldKey,
                        hintText: 'Введите описание',
                        validate: false, forEdit: false,
                      ),
                      DigitalPropertyField(
                        fieldName: 'Бюджет',
                        fieldController: budgetController,
                        // fieldKey: budgetFieldKey,
                        hintText: 'Введите сумму в рублях',
                        validate: false,
                      ),
                      DatePropertyField(
                        fieldName: 'Сроки выполнения',
                        fieldController: dateController,
                        // fieldKey: dateFieldKey,
                        hintText: 'Выберите дату',
                        validate: false,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Text(
                          'О заказчике',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                      ),
                      PropertyField(
                        fieldName: 'Имя заказчика',
                        fieldController: clientController,
                        // fieldKey: clientFieldKey,
                        hintText: 'Введите имя',
                        validate: false, forEdit: false,
                      ),
                      PhonePropertyField(
                        fieldName: 'Контактный номер телефона',
                        fieldController: phoneController,
                        // fieldKey: phoneFieldKey,
                        hintText: 'Введите телефон',
                        validate: false,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: ElevatedButton(
                onPressed: () {
                  if (newProjectFormKey.currentState!.validate()) {
                    Project project = Project(
                      title: titleController.text,
                      description: descriptionController.text,
                      budget: int.tryParse(budgetController.text),
                      date: dateController.text,
                      client: clientController.text,
                      phone: phoneController.text,
                    );
                    bloc.action.add(AddNewProject(project));
                    Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.add,
                      color: Colors.white,
                      opticalSize: 18,
                    ),
                    Text(
                      'Добавить проект',
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall
                          ?.copyWith(color: Colors.white),
                    )
                  ],
                )),
          )
        ],
      ),
    );
  }
}
