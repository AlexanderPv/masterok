import 'package:flutter/material.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/ui/main_screen/add_project_brick.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/ui/projects/available_project_for_list.dart';

class ProjectList extends StatefulWidget {
  const ProjectList({super.key});

  @override
  State<ProjectList> createState() => _ProjectListState();
}

class _ProjectListState extends State<ProjectList> {
  final double outerPadding = 20;
  final double betweenPadding = 12;
  late double shortSize13;
  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
    bloc.action.add(InitApp());
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    shortSize13 = (MediaQuery.of(context).size.width -
            outerPadding * 2 -
            betweenPadding * 2) /
        3;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Проекты'),
        centerTitle: true,
      ),
      body: StreamBuilder<AppState>(
        stream: bloc.state,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return GridView.count(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              crossAxisCount: 3,
              childAspectRatio: 1,
              mainAxisSpacing: betweenPadding,
              crossAxisSpacing: betweenPadding,
              children: [
                AddProjectBrick(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed('/addProject', arguments: bloc);
                    },
                    sideLength: shortSize13),
                ...List.generate(
                    snapshot.data!.projectList.length,
                    (index) => AvailableProjectForList(
                          sizeLength: shortSize13,
                          project: snapshot.data!.projectList[index],
                        ))
              ],
            );
          }
        },
      ),
    );
  }
}
