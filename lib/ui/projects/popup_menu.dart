import 'package:flutter/material.dart';
import 'package:master_ok_new/actions/actions.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/states/app_state.dart';

class PopUpMenu extends StatefulWidget {
  const PopUpMenu({super.key, required this.project, required this.width});

  final Project project;
  final double width;

  @override
  State<PopUpMenu> createState() => _PopUpMenuState();
}

class _PopUpMenuState extends State<PopUpMenu> {
  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppState>(
        stream: bloc.state,
        builder: (context, snapshot) {
          return PopupMenuButton<int>(
            constraints: BoxConstraints(minWidth: widget.width * 0.64),
            icon: const Icon(
              Icons.more_horiz,
              size: 28,
            ),
            itemBuilder: (BuildContext context) => [
              PopupMenuItem<int>(
                value: 0,
                child: const Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text('Редактировать'),
                ),
                onTap: () {
                  Navigator.of(context)
                      .pushNamed('/editProject', arguments: widget.project);
                },
              ),
              const PopupMenuDivider(),
              PopupMenuItem<int>(
                value: 1,
                child: const Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text('Удалить',
                      style: TextStyle(
                          color: Color.fromRGBO(255, 59, 48, 1),
                          fontWeight: FontWeight.w700)),
                ),
                onTap: () {
                  showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (context) {
                      return Dialog(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 20),
                              child: Column(
                                children: [
                                  Text(
                                    'Вы действительно хотите',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                  Text(
                                    'удалить проект?',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      bloc.action
                                          .add(DeleteProject(widget.project));
                                      Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                                      //('/main');
                                    },
                                    child: Container(
                                      height: 42,
                                      decoration: const BoxDecoration(
                                          border: Border(
                                              top: BorderSide(
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.3)),
                                              right: BorderSide(
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.3)))),
                                      child: Center(
                                        child: Text(
                                          'Удалить',
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineSmall
                                              ?.copyWith(
                                                  color: const Color.fromRGBO(
                                                      255, 59, 48, 1)),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Container(
                                      height: 42,
                                      decoration: const BoxDecoration(
                                          border: Border(
                                              top: BorderSide(
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.3)))),
                                      child: Center(
                                        child: Text(
                                          'Отмена',
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleMedium,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  );
                },
              ),
            ],
          );
        });
  }
}
