class ManualTools {
  int? id;
  int manualId;
  int toolId;
  String toolTitle;


  ManualTools(
      {this.id,
        required this.manualId,
        required this.toolId,
        required this.toolTitle
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'manualId': manualId,
      'toolId': toolId,
      'toolTitle': toolTitle
    };
  }

  ManualTools.fromMap(Map<String, dynamic> json)
      : this(
    id: json['id'],
    manualId: json['manualId'],
    toolId: json['toolId'],
    toolTitle: json['toolTitle']
  );
}