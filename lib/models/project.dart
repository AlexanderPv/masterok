
class Project {
  int? id;
  String title;
  String description;
  int? budget;
  String? date;
  String? client;
  String? phone;

  Project(
      {this.id,
      required this.title,
      required this.description,
      this.budget,
      this.date,
      this.client,
      this.phone});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'budget': budget,
      'date': date,
      'client': client,
      'phone': phone,
    };
  }

  Project.fromMap(Map<String, dynamic> json)
  : this(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      budget: json['budget'],
      date: json['date'],
      client: json['client'],
      phone: json['phone']
  );
}
