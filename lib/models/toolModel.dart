
class Tool {
  int? id;
  String title;

  Tool(
      {this.id,
        required this.title,
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
    };
  }

  Tool.fromMap(Map<String, dynamic> json)
      : this(
      id: json['id'],
      title: json['title'],
  );
}