
class Subject {
  int? id;
  String title;

  Subject(
      {this.id,
        required this.title,
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
    };
  }

  Subject.fromMap(Map<String, dynamic> json)
      : this(
      id: json['id'],
      title: json['title'],
  );
}