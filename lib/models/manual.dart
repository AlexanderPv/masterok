
class Manual {
  int? id;
  String title;
  String? description;
  int? complexity;
  String? videoLinks;
  String? mainText;


  Manual(
      {this.id,
        required this.title,
        required this.description,
        this.complexity,
        this.videoLinks,
        this.mainText,
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'complexity': complexity,
      'videoLinks': videoLinks,
      'mainText': mainText,
    };
  }

  Manual.fromMap(Map<String, dynamic> json)
      : this(
      id: json['id'],
      title: json['title'],
      description: json['description'],
    complexity: json['complexity'],
    videoLinks: json['videoLinks'],
    mainText: json['mainText'],
  );
}