class News {
  String picture;
  String title;
  String text;

  News(
      {required this.picture,
        required this.title,
        required this.text});

  News.fromJson(Map<String, dynamic> json)
      : this(
    picture: json['picture'],
    title: json['title'],
    text: json['text'],
  );
}