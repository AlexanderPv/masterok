class ArticleSubjects {
  int? id;
  int articleId;
  int subjectId;
  String subjectTitle;


  ArticleSubjects(
      {this.id,
        required this.articleId,
        required this.subjectId,
        required this.subjectTitle
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'articleId': articleId,
      'subjectId': subjectId,
      'subjectTitle': subjectTitle
    };
  }

  ArticleSubjects.fromMap(Map<String, dynamic> json)
      : this(
    id: json['id'],
      articleId: json['articleId'],
      subjectId: json['subjectId'],
      subjectTitle: json['subjectTitle']
  );
}