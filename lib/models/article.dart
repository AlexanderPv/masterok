
class Article {
  int? id;
  String title;
  String? description;
  String? videoLinks;
  String? mainText;


  Article(
      {this.id,
        required this.title,
        required this.description,
        this.videoLinks,
        this.mainText,
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'videoLinks': videoLinks,
      'mainText': mainText,
    };
  }

  Article.fromMap(Map<String, dynamic> json)
      : this(
      id: json['id'],
      title: json['title'],
      description: json['description'],
    videoLinks: json['videoLinks'],
    mainText: json['mainText'],
  );
}