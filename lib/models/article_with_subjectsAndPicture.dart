import 'package:master_ok_new/models/article.dart';
import 'package:master_ok_new/models/article_subjects.dart';
import 'package:master_ok_new/models/picture.dart';

class ArticleWithSubjectsAndPicture extends Article {
  // int? id;
  // String title;
  // String? description;
  // String? videoLinks;
  // String? mainText;
  List<ArticleSubjects>? articleSubjects;
  Picture? picture;

  ArticleWithSubjectsAndPicture({
    super.id,
    required super.title,
    required super.description,
    super.videoLinks,
    super.mainText,
    this.articleSubjects,
    this.picture
  });
}