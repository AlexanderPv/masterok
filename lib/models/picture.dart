import 'dart:typed_data';
import 'dart:ui';

class Picture {
  int? id;
  int sourceId;
  Uint8List picture;
  String source;

  Picture({
    this.id,
    required this.sourceId,
    required this.picture,
    required this.source,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'sourceId': sourceId,
      'picture': picture,
      'source': source,
    };
  }

  Picture.fromMap(Map<String, dynamic> json)
      : this(
          id: json['id'],
          sourceId: json['sourceId'],
          picture: json['picture'],
          source: json['source'],
        );
}
