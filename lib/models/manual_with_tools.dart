import 'package:master_ok_new/models/toolModel.dart';
import 'package:master_ok_new/models/manual_tools.dart';
import 'package:master_ok_new/models/manual.dart';

class ManualWithTools extends Manual {
  // int? id;
  // String title;
  // String description;
  // int? complexity;
  // String? videoLinks;
  // String? mainText;
  List<ManualTools>? manualTools;

  ManualWithTools(
      {
        super.id,
        required super.title,
        required super.description,
        super.complexity,
        super.videoLinks,
        super.mainText,
        this.manualTools,
      });
}