import 'package:flutter/material.dart';
import 'package:master_ok_new/states/app_state.dart';
import 'package:master_ok_new/blocs/app_bloc.dart';
import 'package:master_ok_new/actions/actions.dart';

class TechnicalScreen extends StatefulWidget {
  const TechnicalScreen({super.key});

  @override
  State<TechnicalScreen> createState() => _TechnicalScreenState();
}

class _TechnicalScreenState extends State<TechnicalScreen> {

  late final AppBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppState>(
      stream: bloc.state,
      builder: (context, snapshot) {
        return Scaffold(
          appBar: AppBar(),
          body: Column(
            children: [
              ElevatedButton(
                  onPressed: (){
                    bloc.action.add(Test());
                  }, child: const Text('Text'))
            ],
          ),
        );
      }
    );
  }
}
