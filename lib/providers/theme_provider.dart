
import 'package:flutter/material.dart';
import 'package:master_ok_new/themes/app_theme.dart';

class ThemeProvider extends ChangeNotifier{

  final ThemeData _currentAppTheme = masterOkTheme;
  ThemeData get currentAppTheme => _currentAppTheme;

  final TextStyle? _h1 = masterOkTheme.textTheme.headlineMedium;
  TextStyle? get h1 => _h1;

  final TextStyle? _b1 = masterOkTheme.textTheme.headlineSmall;
  TextStyle? get b1 => _b1;

  final TextStyle? _b2 = masterOkTheme.textTheme.titleMedium;
  TextStyle? get b2 => _b2;

  final TextStyle? _b3 = masterOkTheme.textTheme.titleSmall;
  TextStyle? get b3 => _b3;

  final TextStyle? _b4 = masterOkTheme.textTheme.bodyMedium;
  TextStyle? get b4 => _b4;
}