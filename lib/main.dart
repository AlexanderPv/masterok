import 'package:flutter/material.dart';
import 'package:master_ok_new/providers/theme_provider.dart';
import 'package:master_ok_new/ui/projects/add_project.dart';
import 'package:master_ok_new/ui/projects//edit_project.dart';
import 'package:master_ok_new/ui/intro/intro1.dart';
import 'package:master_ok_new/ui/main_screen/main_screen.dart';
import 'package:master_ok_new/ui/intro/splash_screen.dart';
import 'package:provider/provider.dart';
import 'package:master_ok_new/ui/projects/about_project.dart';
import 'package:master_ok_new/ui/projects/project_list.dart';
import 'package:master_ok_new/ui/manuals/manuals_list.dart';
import 'package:master_ok_new/ui/manuals/add_manual.dart';
import 'package:master_ok_new/technical_screen.dart';
import 'package:master_ok_new/ui/manuals/about_manual.dart';
import 'package:master_ok_new/ui/manuals/edit_manual.dart';
import 'package:master_ok_new/ui/articles/article_list.dart';
import 'package:master_ok_new/ui/articles/add_article.dart';
import 'package:master_ok_new/ui/articles/about_article.dart';
import 'package:master_ok_new/ui/articles/edit_article.dart';
import 'package:master_ok_new/ui/news/news_list.dart';
import 'package:master_ok_new/ui/news/news_text.dart';
import 'package:master_ok_new/ui/settings/settings.dart';
import 'package:master_ok_new/ui/intro/intro2.dart';
import 'package:master_ok_new/ui/intro/intro3.dart';
import 'package:device_preview/device_preview.dart';

void main() {
  runApp(
      DevicePreview(
          builder: (context) => const MyApp()
      )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => ThemeProvider(),
      child: const MasterOk(
      ),
    );
  }
}

class MasterOk extends StatelessWidget {
  const MasterOk({super.key});

  @override
  Widget build(BuildContext context) {
    var themeProvider = Provider.of<ThemeProvider>(context);
    return MaterialApp(
      useInheritedMediaQuery: true,
      builder: DevicePreview.appBuilder,
      locale: DevicePreview.locale(context),
      title: 'Master Ok',
      theme: themeProvider.currentAppTheme,
      initialRoute: '/splash',
      routes: {
        '/splash': (BuildContext context) => const SplashScreen(),
        '/intro1': (BuildContext context) => const Intro1(),
        '/intro2': (BuildContext context) => const Intro2(),
        '/intro3': (BuildContext context) => const Intro3(),
        '/main': (BuildContext context) => const MainScreen(),
        '/addProject': (BuildContext context) => const AddProject(),
        '/aboutProject': (BuildContext context) => const AboutProject(),
        '/editProject': (BuildContext context) => const EditProject(),
        '/projectList': (BuildContext context) => const ProjectList(),
        '/manualsList': (BuildContext context) => const ManualsList(),
        '/addManual': (BuildContext context) => const AddManual(),
        '/technical': (BuildContext context) => const TechnicalScreen(),
        '/aboutManual': (BuildContext context) => const AboutManual(),
        '/editManual': (BuildContext context) => const EditManual(),
        '/articleList': (BuildContext context) => const ArticleList(),
        '/addArticle': (BuildContext context) => const AddArticle(),
        '/aboutArticle': (BuildContext context) => const AboutArticle(),
        '/editArticle': (BuildContext context) => const EditArticle(),
        '/newsList': (BuildContext context) => const NewsList(),
        '/newsText': (BuildContext context) => const NewsText(),
        '/settings': (BuildContext context) => const Settings(),
      },
    );
  }
}


