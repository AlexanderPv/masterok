import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/models/toolModel.dart';
import 'package:master_ok_new/models/manual.dart';
import 'package:master_ok_new/models/picture.dart';
import 'package:master_ok_new/models/subject_model.dart';
import 'package:master_ok_new/models/article.dart';
import 'dart:io';

abstract class ActionEvent{}
class InitApp extends ActionEvent{}

class AddNewProject extends ActionEvent{
  Project project;
  AddNewProject(this.project);
}

class GetProjects extends ActionEvent{}

class UpdateProject extends ActionEvent{
  Project project;
  UpdateProject(this.project);
}

class DeleteProject extends ActionEvent{
  Project project;
  DeleteProject(this.project);
}

class Test extends ActionEvent{}

class AddNewTool extends ActionEvent{
  Tool tool;
  AddNewTool(this.tool);
}

class InitToolsList extends ActionEvent{}

class GetTools extends ActionEvent{}

class DeleteTool extends ActionEvent{
  Tool tool;
  int index;
  DeleteTool(this.tool, this.index);
}

class SetChooseToolsListItem extends ActionEvent{
  int index;
  bool choose;
  SetChooseToolsListItem(this.index, this.choose);
}

class AddNewManual extends ActionEvent{
  Manual manual;
  AddNewManual(this.manual);
}

class InitManualInfo extends ActionEvent{
  int manualID;
  InitManualInfo(this.manualID);
}

class GetStreamForManualInfo extends ActionEvent{}

class AddPicture extends ActionEvent{
  List<File> pictures;
  AddPicture(this.pictures);
}

class InitManualsList extends ActionEvent{}

class GetSelectedTools extends ActionEvent{}

class ClearSelectedTools extends ActionEvent{}

class DeleteSelectedTool extends ActionEvent{
  int selectedToolId;
  DeleteSelectedTool(this.selectedToolId);
}

class ClearChooseToolsList extends ActionEvent{}

class GetManualWithToolsList extends ActionEvent{}

class DeleteManual extends ActionEvent{
  Manual manual;
  DeleteManual(this.manual);
}

class InitEditManual extends ActionEvent{}

class UpdateManual extends ActionEvent{
  Manual manual;
  UpdateManual(this.manual);
}

/// СТАТЬИ

class InitSubjectList extends ActionEvent{}

class DeleteSubject extends ActionEvent{
  Subject subject;
  int index;
  DeleteSubject(this.subject, this.index);
}

class SetChooseSubjectListItem extends ActionEvent{
  int index;
  bool choose;
  SetChooseSubjectListItem(this.index, this.choose);
}

class AddNewSubject extends ActionEvent{
  Subject subject;
  AddNewSubject(this.subject);
}

class AddNewArticle extends ActionEvent{
  Article article;
  AddNewArticle(this.article);
}

class InitArticleInfo extends ActionEvent{
  int articleId;
  InitArticleInfo(this.articleId);
}

class InitArticleList extends ActionEvent{}

class ClearSelectedSubjects extends ActionEvent{}

class ClearChooseSubjectList extends ActionEvent{}

class GetSelectedSubjects extends ActionEvent{}

class DeleteSelectedSubject extends ActionEvent{
  int selectedSubjectId;
  DeleteSelectedSubject(this.selectedSubjectId);
}

class GetStreamForArticleInfo extends ActionEvent{}

class DeleteArticle extends ActionEvent{
  Article article;
  DeleteArticle(this.article);
}

class InitEditArticle extends ActionEvent{}

class UpdateArticle extends ActionEvent{
  Article article;
  UpdateArticle(this.article);
}