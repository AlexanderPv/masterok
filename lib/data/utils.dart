import 'dart:typed_data';

import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class Validating {
  static String? simpleValidate(String? val) {
    if (val == '') {
      return 'Обязательное поле';
    } else {
      return null;
    }
  }
}

class DateToString {
  static String getDate(DateTime dateTime) {
    String dateString = dateTime.toString();
    String date =
        '${dateString.substring(8, 10)}.${dateString.substring(5, 7)}.${dateString.substring(0, 4)}';
    return date;
  }
}

class RubFormatter {
  static String formatter(int amount) {
    var numForm = NumberFormat("#,###");
    return '${numForm.format(amount).toString().replaceAll(',', ' ')} ₽';
  }
}

