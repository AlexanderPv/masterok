import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/models/subject_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:master_ok_new/models/project.dart';
import 'package:master_ok_new/models/toolModel.dart';
import 'package:master_ok_new/models/manual.dart';
import 'package:master_ok_new/models/manual_tools.dart';
import 'package:master_ok_new/models/picture.dart';
import 'package:master_ok_new/models/article.dart';
import 'package:master_ok_new/models/article_subjects.dart';

class DbQueries {
  late Database db;

  DbQueries._();

  static final DbQueries _instance = DbQueries._();

  static Future<DbQueries> create() async {
    await _instance._initDB();
    return _instance;
  }

  _initDB() async {
    db = await openDatabase(
        join(await getDatabasesPath(), 'masterok_new_database.db'),
        onCreate: (db, version) async {
      await db.transaction((txn) async {
        await txn.execute(
            'CREATE TABLE projects (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, budget INTEGER, date TEXT, client TEXT, phone TEXT)');
        await txn.execute(
            'CREATE TABLE tools (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT)');
        await txn.execute(
            'CREATE TABLE manuals (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, complexity INTEGER, videoLinks TEXT, mainText TEXT)');
        await txn.execute(
            'CREATE TABLE manualTools (id INTEGER PRIMARY KEY AUTOINCREMENT, manualId INTEGER, toolId INTEGER, toolTitle TEXT)');
        await txn.execute(
            'CREATE TABLE pictures (id INTEGER PRIMARY KEY AUTOINCREMENT, sourceId INTEGER, picture BLOB, source TEXT)');
        await txn.execute(
            'CREATE TABLE subjects (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT)');
        await txn.execute(
            'CREATE TABLE articleSubjects (id INTEGER PRIMARY KEY AUTOINCREMENT, articleId INTEGER, subjectId INTEGER, subjectTitle TEXT)');
        await txn.execute(
            'CREATE TABLE articles (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, videoLinks TEXT, mainText TEXT)');
      });
    }, version: 1);
  }

  Future<void> insertProject(Project project) async {
    await db.insert('projects', project.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Project>> getProjectList() async {
    final List<Map<String, dynamic>> maps = await db.query('projects');
    List<Project> pList = List.generate(maps.length, (index) {
      return Project.fromMap(maps[index]);
    });
    return pList;
  }

  Future<List<Manual>> getManualList() async {
    final List<Map<String, dynamic>> maps = await db.query('manuals');
    List<Manual> mList = List.generate(maps.length, (index) {
      return Manual.fromMap(maps[index]);
    });
    return mList;
  }

  Future<void> updateProject(Project project) async {
    await db.update(
      'projects',
      project.toMap(),
      where: 'id = ?',
      whereArgs: [project.id],
    );
  }

  Future<void> deleteProject(int id) async {
    await db.delete(
      'projects',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<void> deleteDataBase() async {
    String path = db.path;
    databaseFactory.deleteDatabase(path);
  }

  Future<void> insertTool(Tool tool) async {
    await db.insert('tools', tool.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Tool>> getToolsList() async {
    final List<Map<String, dynamic>> maps = await db.query('tools');
    List<Tool> tList = List.generate(maps.length, (index) {
      return Tool.fromMap(maps[index]);
    });
    return tList;
  }

  Future<void> deleteTool(int id) async {
    await db.delete('tools', where: 'id = ?', whereArgs: [id]);
    await db.delete('manualTools', where: 'toolId = ?', whereArgs: [id]);
  }

  Future<int> insertManual(Manual manual) async {
    return await db.insert('manuals', manual.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> insertManualTools(ManualTools manualTools) async {
    await db.insert('manualTools', manualTools.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<ManualTools>> getManualToolsList(int manualId) async {
    final List<Map<String, dynamic>> maps = await db
        .query('manualTools', where: 'manualId = ?', whereArgs: [manualId]);
    List<ManualTools> mtList = List.generate(maps.length, (index) {
      return ManualTools.fromMap(maps[index]);
    });
    return mtList;
  }

  Future<void> insertPicture(Picture picture) async {
    await db.insert('pictures', picture.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Picture>> getPictureList(int sourceId, String source) async {
    final List<Map<String, dynamic>> maps = await db.rawQuery(
        'SELECT * FROM pictures WHERE sourceId = "$sourceId" AND source = "$source"');
    List<Picture> pictureList = List.generate(maps.length, (index) {
      return Picture.fromMap(maps[index]);
    });
    return pictureList;
  }

  Future<void> deleteManual(int id) async {
    await db.delete(
      'manuals',
      where: 'id = ?',
      whereArgs: [id],
    );
    await db.delete('manualTools', where: 'manualId = ?', whereArgs: [id]);
    await db.rawQuery('DELETE FROM pictures WHERE sourceId = "$id" AND source = "manual"');
  }

  Future<void> updateManual(Manual manual) async {
    await db.update(
      'manuals',
      manual.toMap(),
      where: 'id = ?',
      whereArgs: [manual.id],
    );
    await db.delete('manualTools', where: 'manualId = ?', whereArgs: [manual.id]);
    await db.rawQuery('DELETE FROM pictures WHERE sourceId = "${manual.id}" AND source = "manual"');
  }

  ///СТАТЬИ


  Future<List<Subject>> getSubjectList() async {
    final List<Map<String, dynamic>> maps = await db.query('subjects');
    List<Subject> sList = List.generate(maps.length, (index) {
      return Subject.fromMap(maps[index]);
    });
    return sList;
  }

  Future<void> deleteSubject(int id) async {
    await db.delete('subjects', where: 'id = ?', whereArgs: [id]);
    await db.delete('articleSubjects', where: 'subjectId = ?', whereArgs: [id]);
  }

  Future<void> insertSubject(Subject subject) async {
    await db.insert('subjects', subject.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<int> insertArticle(Article article) async {
    return await db.insert('articles', article.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> insertArticleSubjects(ArticleSubjects articleSubjects) async {
    await db.insert('articleSubjects', articleSubjects.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Article>> getArticleList() async {
    final List<Map<String, dynamic>> maps = await db.query('articles');
    List<Article> arList = List.generate(maps.length, (index) {
      return Article.fromMap(maps[index]);
    });
    return arList;
  }

  Future<List<ArticleSubjects>> getArticleSubjectList(int articleId) async {
    final List<Map<String, dynamic>> maps = await db
        .query('articleSubjects', where: 'articleId = ?', whereArgs: [articleId]);
    List<ArticleSubjects> asList = List.generate(maps.length, (index) {
      return ArticleSubjects.fromMap(maps[index]);
    });
    return asList;
  }

  Future<void> deleteArticle(int id) async {
    await db.delete(
      'articles',
      where: 'id = ?',
      whereArgs: [id],
    );
    await db.delete('articleSubjects', where: 'articleId = ?', whereArgs: [id]);
    await db.rawQuery('DELETE FROM pictures WHERE sourceId = "$id" AND source = "article"');
  }

  Future<void> updateArticle(Article article) async {
    await db.update(
      'articles',
      article.toMap(),
      where: 'id = ?',
      whereArgs: [article.id],
    );
    await db.delete('articleSubjects', where: 'articleId = ?', whereArgs: [article.id]);
    await db.rawQuery('DELETE FROM pictures WHERE sourceId = "${article.id}" AND source = "article"');
  }
}
