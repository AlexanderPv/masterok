import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:master_ok_new/models/news_model.dart';

Future<List<News>> getNewsFromJson(String st) async{
  final String data = await rootBundle.loadString(st);
  final jsonData = jsonDecode(data);
  List<News> news = jsonData.map<News>((e) => News.fromJson(e)).toList();
  return news;
}